\chapter{Espacios de diferenciales cuadr\'aticas}

En este cap\'itulo utilizaremos las clases silla estandar y los per\'iodos de estas para construir
parametrizaciones naturales de familias de diferenciales cuadr\'aticas.


\section{Tipo de descomposici\'on en franjas horizontales}

El objetivo de esta secci\'on es describir las familias de diferenciales cuadr\'aticas para las
que posteriormente definiremos una parametrizaci\'on.

En lo sucesivo consideraremos \'unicamente diferenciales cuadr\'aticas GMN sin polos 
de orden mayor que \( 2\), y dado que hablaremos de distintas diferenciales simult\'aneamente,
adoptaremos la notaci\'on \( (X,\varphi)\) para hablar de una diferencial sobre la superficie \( X\).

\begin{definicion}
Diremos que dos diferenciales cuadr\'aticas \( (X,\varphi)\) y \( (Y,\vartheta)\)
tienen el mismo \emph{tipo de descomposici\'on (en franjas)} cuando exista un difeomorfismo \( f:X \rightarrow Y\)
que lleve polos en polos, ceros en ceros y trayectorias separatrices en trayectorias separatrices y que, por lo tanto,
lleve franjas horizontales en franjas horizontales. Al difeomorfismo \( f\) con las propiedades mencionadas, 
le llamaremos una equivalencia de franjas.
\end{definicion}

El conjunto de diferenciales cuadr\'aticas que en principio nos interesar\'ia describir es el conjunto de todas
las diferenciales cuadr\'aticas con un mismo tipo de descomposici\'on.
Adem\'as, uno de los objetivos principales ser\'ia dotar de una topolog\'ia a dicho conjunto,
y de ser posible, que con dicha topolog\'ia admita otras estructuras geom\'etricas compatibles, por ejemplo
la estructura de variedad compleja.

El problema con este conjunto es que los intentos m\'as naturales de dotarlo de una topolog\'ia
producen un espacio con singularidades, es decir, en vez de obtener una variedad compleja, el resultado es un orbifold complejo.

Este problema emana del hecho de que cuando dos diferenciales cuadr\'aticas tienen el mismo tipo de descomposici\'on en franjas,
en general no es posible escoger naturalmente una equivalencia de franjas. Equivalentemente, el problema surge
pues una diferencial cuadr\'atica tiene autoequivalencias no triviales.

Hay varias maneras equivalentes de resolver este hecho. Una de ellas es considerar diferenciales cuadr\'aticas
con un poco de estructura adicional:

\begin{definicion}\label{def:marco-homologico}
Sea \( (X,\varphi)\) una diferencial cuadr\'atica. Un \( \Gamma-\)marco \( \theta \) para la diferencial
es un isomorfismo de grupos
\[ \theta:\Gamma\rightarrow \widehat{\H}(\varphi)\]
donde claramente \( \Gamma \cong\mathbb{Z}^{N(\varphi)}\).
Sean \( (X_1,\varphi_1)\) y \( (X_2,\varphi_2)\)  dos diferenciales con marcos \( \theta_1\) y \( \theta_2\)
y sea  \( f:X_1\rightarrow X_2\) un isomorfismo complejo tal que \( f^*(\varphi_2)=\varphi_1\).
Diremos que \( f\) preserva los marcos
 cuando el levantamiento distinguido \( f:\widehat X_{\varphi_1} \rightarrow \widehat X_{\varphi_2}\) que preserva las
\( 1-\)formas tautol\'ogicas haga conmutar el diagrama:
\begin{center}
\begin{tikzpicture}
    \node (A) at (0,0) {$\widehat \H (\varphi_1)$};
    \node (B) at (3,0) {$\widehat \H (\varphi_2)$};
\node (C) at (1.5,2) {$\Gamma$};
    \draw[transform canvas={yshift=0.3ex},->] (C) -- node[above] {$ \theta_1 $} (A);
    \draw[->] (C) -- node[auto] {$  \theta_2  $} (B);
    \draw[->] (A) -- node[below] {$  \widehat f_*  $} (B);
\end{tikzpicture} 
\end{center}
\end{definicion}

El concepto de tipo de descomposici\'on en  franjas se puede extender a diferenciales con marcos,
y con este concepto, se puede demostrar
que el conjunto de diferenciales con marcos con un mismo tipo de descomposici\'on en franjas es una variedad compleja, que a su vez
es un cubriente del orbifold ya discutido.

Este enfoque es el que se adopta en el art\'iculo \cite{bridgeland2015}.
En este texto utilizaremos otro m\'etodo, que es totalmente equivalente, pero se adapta mejor a las necesidades del presente trabajo.
En la siguiente secci\'on demostraremos la equivalencia entre ambos m\'etodos.

En lo sucesivo, fijemos una diferencial cuadr\'atica \( (X_0,\varphi_0)\).

\begin{definicion}
Una diferencial \emph{\( \varphi_0-\)marcada} es una diferencial cuadr\'atica \( (X,\varphi)\) junto con una equivalencia de franjas
\( f:X_0\rightarrow X\).
\end{definicion}

Al igual que con la definici\'on anterior, necesitamos una manera de determinar
cuando una transformaci\'on preserve los \( \varphi_0-\)marcos.


\begin{definicion}
	Sean \( (X_1, \varphi_1, f_1)\) y \( (X_2, \varphi_2, f_2)\) dos diferenciales \( \varphi_0-\)marcadas.
	Sea \( g:X_1 \rightarrow X_2\) una equivalencia de franjas. Diremos que \( g\) es una equivalencia de franjas
	\emph{marcada} cuando la transformaci\'on \( f_2^{-1}\circ g\circ f_1\) (que preserva la descomposici\'on en franjas de \( \varphi_0\)) fije las franjas, es decir, la imagen de cada franja de \( \varphi_0\) es ella misma.

	As\'i mismo, cuando \( g:(X_1, \varphi_1):\rightarrow (X_2,\varphi_2)\) sea un isomorfismo de diferenciales cuadr\'aticas
	y adem\'as sea una equivalencia de franjas marcada, diremos que es un isomorfismo entre \( (X_1, \varphi_1, f_1)\) y \( (X_2, \varphi_2, f_2)\).
\end{definicion}

Cabe se\~nalar que esta manera de abordar el problema de construir el espacio de diferenciales cuadr\'aticas
est\'a inspirado directamente en una de las posibles construcciones del espacio de Teichm\"uller de superficies de Riemann de g\'enero \( g\). V\'ease \cite[Secci\'on~6.4]{hubbard2006teichmuller}.


Denotaremos al conjunto de clases de isomorfismo de  diferenciales \( \varphi_0-\)marcadas por \( \Cuad(X_0, \varphi_0)\),
es decir:

\[ \Cuad(X_0, \varphi_0):= \left\{\vphantom{\frac12} (X,\varphi,f)\, \middle| \, (X,\varphi,f)\mbox{ es una diferencial }\varphi_0-\mbox{marcada}  \right\}/ \cong\]

Notemos que existe un punto distinguido en \( \Cuad(X_0, \varphi_0)\): \( (X_0,\varphi_0, \Id)\).

Al finalizar este cap\'itulo demostraremos que \( \Cuad(X_0, \varphi_0)\) est\'a en biyecci\'on con
el conjunto \( \mathbb{H}_+^{N(\varphi)}\)  mediante la \emph{aplicaci\'on de per\'iodos},
donde \( \mathbb H_+ =\{z\in\mathbb{C} | \im(z)>0\}\) es el semiplano superior abierto.

\section{Coordenadas de per\'iodos en una celda}

Antes de poder definir la aplicaci\'on de per\'iodos, precisaremos una construcci\'on
relacionada con las equivalencias de franjas marcadas.

Sean \( (X_1, \varphi_1, f_1)\) y \( (X_2, \varphi_2, f_2)\) dos diferenciales \( \varphi_0-\)marcadas
y sea \(g:X_1 \rightarrow X_2 \) una equivalencia de franjas marcada. Dado que \( g\) preserva los
puntos de ramificaci\'on de los respectivos cubrientes espectrales, \( g\) se puede levantar a dichos cubrientes.

Como los cubrientes espectrales son cubrientes de dos hojas, existen exactamente dos maneras (en principio indistinguibles)
de obtener el levantamiento de \( g\).

Sea \( \hat g\) cualquiera de dichos levantamientos y sea \( F\) una franja horizontal de \( X_1\).
Sea \( \gamma\) la conexi\'on silla de \( F\). Como la franja es topol\'ogicamente trivial y \( g\) preserva franjas, concluimos que \( g(\gamma)\) es homot\'opica a la conexi\'on silla de \( g(F)\).
Esto implica que el levantamiento \( \hat g\) satisface \( \hat g_*(\alpha_f)=\pm \alpha_{g(F)}\).

\begin{proposicion}\label{prop:levantamiento-distinguido}
	Sean \( F_1,\ldots,F_N\) las franjas horizontales de \( (X_1,\varphi_1)\) y 
	\( \alpha_1,\ldots,\alpha_N\) las clases silla estandar correspondientes.
	Uno de los levantamientos de \( g\), al que denotaremos \( \hat g_+\) satisface:
	\[ \hat g_+ (\alpha_i)= +\alpha_{g(F_i)}\]
	para toda clase silla estandar, mientras que el otro posible levantamiento,
	que denotaremos \( \hat g_-\) satisface:
	\[ \hat g_- (\alpha_i)= -\alpha_{g(F_i)}.\]

	A \( \hat g_+\) le llamaremos el \emph{levantamiento distinguido} de \( g\) y lo
	denotaremos usualmente por \( \hat g\).


\end{proposicion}

%transporte paralelo en una celda y identificacion con un \'unico espacio tangente, la distribucion de lineas se transporta
%a la misma linea en el tangente unico.

%hay una equivalencia entre:
%una curva orientada que conecta los ceros de la franja
%cualquier vector transversal a la foliaci\'on
%un campo vectorial que hace el mismo angulo no nulo con la foliacion en todo punto
%una eleccion de una hoja

%hay una forma de mandar un vector transversal a la foliaci\'on a otro vector transversal a la foliaci\'on en la superficie destino:
%a partir del vector se construye un campo transversal. como el mapeo preserva las curvas separatrices y es un difeo
%entonces preserva el complemento de la distribucion en esos puntos.

%ahora bien, empezamos con un vector transversal, completamos a un campo transversal, lo extendemos usando el transporte paralelo
%hasta completar una vecindad de algun punto en la frontera de la franja (esto puede no dar un campo bien definido en la franja union bola, pero da un campo bien definido en la bola, que en la semibola inferior es compatible con el campo en la franja)

%este nuevo campo, en el punto frontera, es transversal y su imagen es transversal, como esta propiedad es abierta, en una vecindad del punto es transversal, es decir, se queda de "un solo lado" de la distribucion. Tomando un punto en la semibola inferior, pero suficientemente cerca, podemos tomar un vector transversal que define una direcci\'on en la franja destino. esta es la imagen del vector buscado.

%que  el levantamiento mande la clase silla estandar en la clase silla estandar es equivalente a que 
%al mandar un vector con el metodo anterior, la hoja que determina su imagen es la imagen de la hoja determinada por el vector.

%ahora bien, tambien hay una manera de transportar la direcci\'on en una franja a una franja adyacente, esto es simplemente usando transporte paralelo a lo largo de una curva escogida. es facil ver que la hoja correspondiente al traslado de un vector es la hoja trasladada del vector original

%ademas, si tenemos una bola en la frontera y definimos con trasnporte paralelo un campo en la bola, de cada lado de la frontera este
%campo determina dos direcciones tal que una es transporte de la otra.
%pero la imagen de una bola quiza mas chica cumple que ahi el campo vectorial imagen es transversal tambien, por lo que define dos
%campos vectoriales en la franja imagen y su adyacente, que son uno el traslado del otro, y que ambos son imagen de los del dominio

%esto implica que el cubo conmuta en todas las caras salvo posiblemente las laterales, lo cual implica que si una lateral conmuta, necesariamente la otra tambi\'en.

%esto demuestra por conexidad, que si un levantamiento manda una clase silla en la clase silla correspondiente, entonces manda toda base
%en la base correspondiente




\subsection{Aplicaci\'on de per\'iodos}

A partir de este punto, no s\'olo fijaremos la diferencial cuadr\'atica
\( (X_0, \varphi_0)\), sino que fijaremos tambi\'en una enumeraci\'on
de las franjas horizontales de \( \varphi_0\).

Sean \( F_1,\ldots,F_N\) las \( N:=N(\varphi_0)\) franjas horizontales de \( \varphi_0\),
y sean \( \alpha_i:= \alpha_{F_i}\) las \( N\) clases silla estandar correspondientes a
las franjas horizontales.

\begin{definicion}
	La \emph{aplicaci\'on de per\'iodos} definida sobre el conjunto de
	diferenciales cuadr\'aticas \( \varphi_0-\)marcadas es la aplicaci\'on:
	\begin{align*}
		Per: \Cuad(X_0,\varphi_0) & \xrightarrow{~~~~~~} \mathbb{H}_+^N\\
	(X,\varphi, f) &\mapsto  (\hat\varphi (\hat f_*\alpha_i))_{i=1}^N
\end{align*}
donde \( \hat f :\hat X_0 \rightarrow \hat X\) es el levantamiento distinguido de \(f \)
que discutimos anteriormente y \( \hat \varphi:\hatH (\varphi)\rightarrow \mathbb{C}\) 
es el per\'iodo de \(\varphi\).%
\tablefootnote{V\'ease la p\'agina \pageref{pag:periodo}}

Recordemos que por la construcci\'on de las clases silla estandar, se satisface que
si \( \alpha\) es cualquier clase silla estandar, 
\( \hat \varphi(\alpha)\in \mathbb{H}_+\)
y como \( \hat f_*(\alpha_i)\) es tambi\'en una clase silla estandar, concluimos que
efectivamente la imagen de \( Per\) est\'a contenida en \( \mathbb{H}_+^N\).
\end{definicion}

Durante el resto del cap\'itulo desarrollaremos la demostraci\'on del siguiente teorema:

\begin{teorema}\label{teo:principal}
Sea \( \varphi_0\) una diferencial cuadr\'atica sobre una superficie de Riemann \( X_0\)
con ceros simples, polos dobles, por lo menos un cero y un polo y sin trayectorias silla.
Escojamos cualquier enumeraci\'on de las franjas horizontales de \( (X_0,\varphi_0)\).
Entonces, la aplicaci\'on de per\'iodos resultante \( Per:\Cuad(X_0,\varphi_0)\rightarrow \mathbb{H}_+^N\) es una biyecci\'on.
\end{teorema}


\subsection{Diferenciales \( \varphi_0-\)marcadas y \( \Gamma-\)marcadas}

Como mencionamos anteriormente, el m\'etodo que estamos usando para <<marcar>> las diferenciales
cuadr\'aticas es distinto al que se emplea en \cite{bridgeland2015}.
En esta subsecci\'on exploraremos a detalle la relaci\'on entre ambos m\'etodos y demostraremos su equivalencia.

Recordemos que en la definici\'on de diferenciales \( \Gamma-\)marcadas (definici\'on \ref{def:marco-homologico}) definimos qu\'e es que dos diferenciales \( \Gamma-\)marcadas sean isomorfas,
sin embargo, ahora necesitamos decir exactamente qu\'e es que dos diferenciales \( \Gamma-\)marcadas
tengan el mismo tipo de descomposici\'on en franjas.

Antes de proseguir, recordemos que una consecuencia de la proposici\'on \ref{prop:dimension-h-gorro} es que
el rango del grupo de homolog\'ia gorro de una diferencial \( (X,\varphi)\), es decir \( N(\varphi)\), s\'olo depende del
\'genero de la superficie \( X\) y 
\emph{tipo polar} de \( \varphi\), es decir, la colecci\'on no ordenada de enteros dada por los
ordenes de los polos de \( \varphi\). Esto quiere decir que si dos diferenciales cuadr\'aticas
est\'an definidas sobre superficies del mismo g\'enero y tienen el mismo tipo polar,
necesariamente tienen grupos de homolog\'ia gorro isomorfos. Otra manera de decir esto es que
dichas diferenciales admiten \( \Gamma-\)marcos, donde \( \Gamma = \mathbb{Z}^{N}\).
Esto nos permite hacer la siguiente definici\'on:

\begin{definicion}
Sea \( g\) un entero no negativo y \( m=\{m_1,\ldots,m_d\}\) una coleci\'on no ordenada de enteros positivos.
Sea \( \Gamma=\mathbb{Z}^N\) donde \( N\) est\'a dado por la f\'ormula de la proposici\'on \ref{prop:dimension-h-gorro} tomando a \( g\) como el g\'enero y a \( m\) como los ordenes de los polos.
El conjunto de diferenciales GMN \( \Gamma-\)marcadas es el conjunto:
\[ \Cuad(g,m)^\Gamma:=\raisebox{-.8em}{\( \bigslant{\left\{(X,\varphi, \theta) \middle\vert \begin{array}{l}
			g(X)=g, \varphi \text{ es GMN y de tipo polar }m\\
			\text{ y }%
		\theta:\Gamma \rightarrow \hatH(\varphi)\text{ es un isomorfismo}\end{array}%
\right\}}{\cong} \)}\]

Tambi\'en definimos \( \Cuad(g,m)\) como:
\[ \Cuad(g,m):=\raisebox{-.8em}{\(  \bigslant{\left\{(X,\varphi) \middle\vert \begin{array}{l}
			g(X)=g, \varphi \text{ es GMN y de tipo polar }m\\
       \end{array}%
\right\}}{\cong} \)}\]
\end{definicion}

Es claro que el grupo \( \Aut(\Gamma)\) actua en \( \Cuad(g,m)^\Gamma\) y que el cociente bajo dicha
acci\'on es isomorfo a \( \Cuad(g,m)\).
Denotemos por \( p:\Cuad(g,m)^\Gamma\rightarrow\Cuad(g,m)\) la proyeci\'on al cociente,
que de hecho coincide con la aplicaci\'on que olvida el \( \Gamma-\)marco.

En el subconjunto de \( \Cuad(g,m)^\Gamma\) de diferenciales sin trayectorias silla, la noci\'on
de tipo de descomposici\'on en franjas tiene sentido: 
Diremos que dos diferenciales \( \Gamma-\)marcadas \( (X_1,\varphi_1,\theta_1)\) y \( (X_2,\varphi_2,\theta_2)\)
tienen el mismo tipo de descomposici\'on en franjas cuando exista un difeomorfismo \( g:X_1\rightarrow X_2\)  que preserva franjas y tal que el levantamiento distinguido \( \hat g\) conmuta con los marcos,
es decir, es la misma propiedad que cumplen los isomorfismos de diferenciales \( \Gamma-\)marcadas,
s\'olo que en este caso el difeomorfismo no es isomorfismo, y el levantamiento distinguido existe
por la proposici\'on \ref{prop:levantamiento-distinguido}.

Sea \( U\subseteq \Cuad(g,m)\) el conjunto de diferenciales con el mismo tipo de descomposici\'on
en franjas y sea \( U^\Gamma := p^{-1}(U)\subseteq \Cuad(g,m)^\Gamma\).
Sea \( (X,\varphi, \theta)\in U^\Gamma\). Denotemos por \( \Cuad(X,\varphi,\theta)^\Gamma\) al
conjunto de diferenciales \( \Gamma-\)marcadas que tienen el mismo tipo de descomposici\'on
en franjas que \( (X,\varphi,\theta)\). Es claro que \( \Cuad(X,\varphi,\theta)^\Gamma\) se proyecta
al conjunto \( U\). Adem\'as, es claro que la uni\'on de las im\'agenes de \( \Cuad(X,\varphi,\theta)^\Gamma\) bajo la acci\'on de \( \Aut(\Gamma)\) es todo \( U\).

En \cite{bridgeland2015} se demuestra que \( \Cuad(g,m)^\Gamma\) es naturalmente una
variedad compleja y \( \Cuad(g,m)\) un orbifold complejo. Para relacionar este formalismo
con el de diferenciales \( \varphi-\)marcadas utilizaremos la siguiente proposici\'on:
\begin{proposicion}\label{prop:gamma-phi}
	Sea \( (X_0,\varphi_0,\theta_0)\in \Cuad(g,m)^\Gamma\). Entonces
	hay una biyecci\'on natural entre \( \Cuad(X_0,\varphi_0,\theta_0)^\Gamma\) y
	\( \Cuad(X_0,\varphi_0)\).
\end{proposicion}
\begin{proof}
	Sea \( (X,\varphi,f)\in\Cuad(X_0,\varphi_0) \). Recordemos que por la proposici\'on \ref{prop:levantamiento-distinguido}
	\( f\) tiene un levantamiento distinguido a los cubrientes espectrales de \( X_0\) y \( X\)
	que adem\'as induce un isomorfismo en los correspondientes grupos de homolog\'ia gorro.
	Denotemos dicho isomorfismo por \( \hat f_*\). De este modo \( \hat f_*\circ\theta\) es un
	\( \Gamma-\)marco para \( (X,\varphi)\). Se puede ver que \( (X, \varphi, \hat f_*\circ\theta) \in \Cuad(X_0,\varphi_0,\theta_0)^\Gamma\) y que la diferencial \( \Gamma-\)marcada resultante
	est\'a bien definida.

	Inversamente, sea \( (X, \varphi, \theta) \in \Cuad(X_0,\varphi_0,\theta_0)^\Gamma\).
	Entonces existe un difeomorfismo \( f:X_0 \rightarrow X\) que preserva franjas y
       	tal que su levantamiento
	distinguido preserva los \( \Gamma-\)marcos. Es claro que \( (X,\varphi, f)\)
	es una diferencial \( \varphi_0-\)marcada. El punto importante aqu\'i es demostrar que
	dicha diferencial no depende del difeomorfismo \( f\) escogido. Para esto,
	supongamos que \( g\) es otro difeomorfismo con las mismas propiedades ya mencionadas.
	Entonces \( (X,\varphi, f)\) y \( (X,\varphi, g)\) definen diferenciales \( \varphi_0-\)marcadas posiblemente distintas. Para ver que efectivamente definen la misma diferencial, demostraremos que tienen el mismo vector de per\'iodos y apelaremos al teorema \ref{teo:principal}.
	Sea \( F_i\) la \( i-\)\'esima franja horizontal de \( X_0\). Para ver que el \( i-\)\'esimo
	periodo de las dos diferenciales es el mismo, basta ver que \( f(F_i)\) y \( g(F_i)\)
	son la misma franja de \( X\). Sea \( \alpha_i\) la clase silla estandar correspondiente a
	\( F_i\) y sea \( a:= \theta_0^{-1}(\alpha_i)\) el elemento correspondiente en \( \Gamma\).
	Como los levantamientos de \( f\) y de \( g\) preservan los \( \Gamma-\)marcos,
	es claro que \( \hat f_*(\alpha_i) = \hat f_*(\theta_0(a))=\theta(a)= \hat g_*(\theta_0(a))= \hat g_*(\alpha_i)\) por lo que \( \hat f_*(\alpha_i)= \hat g_*(\alpha_i)\) y las franjas correspondientes tambi\'en son iguales, es decir, \( f(F_i)=g(F_i)\).
\end{proof}


\subsection{Pegado de franjas horizontales}

En esta secci\'on demostramos el teorema \ref{teo:principal}.
Dividiremos la demostraci\'on en dos partes: demostrar la inyectividad y demostrar la suprayectividad.

Para demostrar la suprayectividad, utilizaremos un tipo de <<cirug\'ia>> que nos permitir\'a obtener
una diferencial cuadr\'atica con per\'iodos arbitrarios (en \( \mathbb{H}_+^N\)).


Construiremos las diferenciales cuadr\'aticas tomando como punto de partida el punto distinguido 
de \( \Cuad(X_0,\varphi_0)\), es decir, \( (X_0,\varphi_0,\Id)\).

Sea \( V\in \mathbb{H}_+^N\) un vector arbitrario. Queremos encontrar una superficie \( \varphi_0-\)marcada que tenga como vector de per\'iodos
a \( V\).

Como primer paso, trabajaremos invdividualmente con cada una de las franjas de \( (X_0,\varphi_0)\).

Sin perdida de generalidad, sea \( F\) la primera franja horizontal de \( X_0\).

En este caso ser\'a m\'as util trabajar con franjas estandar en las que uno de los puntos marcados es el 0. Toda franja estandar se puede llevar a una \'unica franja de este tipo mediante una traslaci\'on
en el plano complejo. De esta forma la franja ya no es sim\'etrica con respeto al origen,
sino con respecto al punto \( w/2\), donde \( w\) es el per\'iodo de la franja.

Sea \( f\) cualquier isomorfismo de la franja horizontal con una franja estandar \( F_w\).
Y sea \( F_{v_1}\) la franja estandar de per\'iodo \( v_1\).

Afirmamos que existe un difeomorfismo \( g:F_{v_1} \rightarrow F_w\) que cumple las siguientes propiedades:
\begin{itemize}
		 \item en vecindades de la frontera de \( F_{v_1}\) es una traslaci\'on del plano complejo;
		 \item conmuta con traslaciones en la direcci\'on horizontal;
		 \item fija al cero y lleva a \( v_1\) en \( w\).
\end{itemize}

Para construir dicho difeomorfismo, primero consideremos cualquier funci\'on
\( h:[0,\im(v_1)] \rightarrow [0,1]\) que sea lisa, creciente, y tal que
tal que es identicamente cero en una vecindad de \(0\) y uno en una vecindad de \( \im(v_1)\).

Usando la funci\'on \( h\) construimos el difeomorfismo mencionado con la f\'ormula:

\[g(z):= z+ (w-v_1)h(\im(z)) \]

Es claro que \( g\) as\'i definido cumple las propiedades buscadas.

Sea \( \psi= f\circ g:F_{v_1}\rightarrow X_0\).
Entonces \( \psi\) es un difeomorfismo entre las franja horizontal estandar
de per\'iodo \( v_1\) y la primera franja horizontal de \( X_0\). Este difeomorfismo preserva la foliaci\'on horizontal, sin embargo, es claro que en general no es isomorfismo complejo ni preserva la diferencial cuadr\'atica.
A pesar de esto, \( \psi\) restringido a vecindades suficientemente peque\~nas de la frontera
de \( F_{v_1}\) s\'i
es un isomorfismo complejo y preserva la diferencial cuadr\'atica.

Usando el difeomorfismo \( \psi\), copiamos la diferencial cuadr\'atica y la estructura compleja
de la franja horizontal estandar a la superficie \( X_0\).
De este modo obtenemos una nueva estructura compleja y una nueva 
diferencial cuadr\'atica definidas en \( F\) y tal que \( \psi^{-1}\) es un isomorfismo con
la franja horizontal estandar de per\'iodo \( v_1\).

Repitiendo este procedimiento para todas las dem\'as franjas y los per\'iodos \( v_i\),
obtenemos nuevas estructuras complejas y diferenciales cuadr\'aticas definidas en cada franja horizontal de \( X_0\).

El problema ahora es demostrar que dichas estructuras son compatibles y definen una \'unica estructura compleja sobre toda la superficie \( X_0\) y una diferencial cuadra\'tica meromorfa con los mismos ceros y polos
que la diferencial \( \varphi_0\).

Notemos que cada una de estas nuevas estructuras son iguales a la estructura vieja en vecindades de todas las trayectorias silla. Como son iguales a la estructura vieja, son compatibles entre si en vecindades suficientemente peque\~nas de las trayectorias silla y los puntos cr\'iticos finitos.

Esto implica que las distintas estructuras complejas son compatibles en el abierto \( X_0^\circ\) y definen una estructura de superficie de Riemann sobre esta subsuperficie.
De esta forma, \'unicamente resta ver que la estructura compleja se extienden a los polos de \( \varphi_0\). Esto es equivalente a demostrar que en la nueva estructura compleja, peque\~nas vecindades perforadas de los polos son biholomorfas a discos perforados.

\begin{lema}\label{lema:extension-polos}
	Sea \( X^\circ_\mathcal{N}\) la subsuperficie con la nueva estructura compleja
	y sea \( p\) un polo de \( \varphi_0\).
	Existe una vecindad perforada de \( p\) en \( X^\circ_\mathcal{N}\) que es biholomorfa
	a un disco perforado \( \mathbb{D}\setminus \{0\}\).
\end{lema}
\begin{proof}
	La demostraci\'on utiliza la clasificaci\'on de posibles estructuras complejas sobre \( \mathbb{D}\setminus\{0\}\). Para esto apelaremos a resultados que se mencionan en la \'ultima secci\'on del cap\'itulo, espec\'ificamente a la definici\'on del m\'odulo de un cilindro complejo (definici\'on \ref{def:modulo}) y a
	la noci\'on de aplicaci\'on cuasiconforme (definici\'on \ref{def:casiconforme}).

	Sea \[ U:=\bigcup_{\overline{F_i}\ni p}^N F_i\] la uni\'on de todas las franjas horizontales que tienen a \( p\) en su cerradura.
	Es claro que \( U\) es una vecindad de \( p\).

	Sea \( D\) un disco suficientemente peque\~no centrado en \( p\) y contenido en \( U\).
	Entonces es claro que \( D\setminus \{p\}\) en la estructura compleja vieja 
	es biholomorfo a un disco perforado. Por el lema \ref{lema:modulo-invariante},
	tiene modulo infinito.

	Denotaremos a \( U\) con la estructura compleja nueva por \( U_\mathcal{N}\)
	y con la estructura compleja vieja por \( U_\mathcal{V}\).
	
	La identidad vista como aplicaci\'on \( \Id: U_\mathcal{V}\rightarrow U_\mathcal{N}\) es una aplicaci\'on
	cuasiconforme. Para ver esto basta ver que es cuasiconforme en cada franja horizontal
	pues la proposici\'on \ref{prop:pegado-casiconforme} garantiza que en ese caso,
	es cuasiconforme en la uni\'on.

	Para ver que la identidad es cuasiconforme en cada franja, notemos que es equivalente al
	difeomorfismo \( \psi\) antes definido con el que construimos la nueva estructura compleja 

	Como \( \psi\) es diferenciable y conmuta con la traslaci\'on en la direcci\'on horizontal,
	para calcular su dilataci\'on basta tomar el supremo de las dilataciones puntuales en cualquier recta vertical.
	Como la franja est\'a acotada en la direcci\'on vertical, concluimos que \( \psi\) tiene dilataci\'on acotada y por lo tanto es cuasiconforme. Esto prueba que \( \Id: U_\mathcal{V}\rightarrow U_\mathcal{N}\) es cuasiconforme, y por lo tanto tambien su restricci\'on al disco \( D\setminus\{p\}\).

	Esto implica que los m\'odulos de \( D\setminus \{p\}\) con la vieja y la nueva estructura compleja satisfacen:
	\[ \frac 1 K m\big(D_\mathcal{N}\!\setminus\!\{p\}\big) ~~\leq~~ m\big(D_\mathcal{V}\!\setminus\! \{p\}\big) ~~\leq~~ K m\big(D_\mathcal{N}\!\setminus\! \{p\}\big)\]
	donde \( K\) es una constante positiva. Como \( m\big(D_\mathcal{V}\setminus \{p\}\big)= \infty\), concluimos que \( m\big(D_\mathcal{N}\setminus \{p\}\big)=\infty\)
	es decir, \( D_\mathcal{N}\setminus \{p\}\) es biholomorfo a \( \mathbb{D}\setminus \{0\}\),
	que es lo que se quer\'ia demostrar.
\end{proof}

Con esto demostramos que la nueva estructura compleja de \( X_0^\circ\) se extiende a toda la superficie. De manera similar, se puede demostrar que las diferenciales cuadr\'aticas son compatibles
y definen una diferencial cuadr\'atica holomorfa en \( X_0^\circ\). Por la forma de las trayectorias
cerca de los polos, es claro que esta nueva diferencial cuadr\'atica se extiende a toda la superficie  \( X_0\) y que
tiene polos dobles exactamente en los polos dobles de \( \varphi_0\).
As\'i, hemos obtenido una nueva diferencial \( \varphi_0-\)marcada \( (X_\mathcal{N},\varphi_N,\Id)\)
y por construcci\'on, su vector de per\'iodos es exactamente \( V\).
Esto prueba la suprayectividad de
la aplicaci\'on de per\'iodos.

Para completar la demostraci\'on del teorema, resta ver que la aplicaci\'on de per\'iodos
es inyectiva. Para esto, sean \( (X_1,\varphi_1, f_1)\) y \( (X_2,\varphi_2, f_2)\)
dos diferenciales \( \varphi_0\) marcadas y que tienen los mismos per\'iodos.

Necesitamos construir un isomorfismo entre \( (X_1,\varphi_1)\) y \( (X_2,\varphi_2)\) que adem\'as sea isomorfismo \( \varphi_0-\)marcado, es decir,
construir un isomorfismo  \( g:X_1\rightarrow X_2\) tal que para cada franja \( F\) de \(X_0\),
se satisface que \( g\circ f_1(F)\) y \( f_2(F)\) son la misma franja.

De manera similar a como demostramos la suprayectividad de la aplicaci\'on de per\'iodos,
construiremos dicho isomorfismo trabajando individualmente en cada franja de \( X_1\).
Para esto, sea \( F\) cualquier franja de \( X_0\) y sean \( F_1:=f_1(F)\) y \( F_2:=f_2(F)\) las franjas respectivas en \( X_1\) y \( X_2\).
Escojamos una curva orientada \( \gamma\) que conecta los dos puntos cr\'iticos en la frontera de \( F\),
y consideremos su imagen en \( X_1\) y \( X_2\).

De este modo, existe un \'unico isomorfismo \(\psi_1 \) entre \( F_1\) y \( F_w\) donde \( w\) es el per\'iodo de \( F_1\) tal que manda los puntos cr\'iticos en los puntos marcados y adem\'as
manda la curva escogida en una curva homot\'opica al segmento orientado que conecta el cero con \( w\).

An\'alogamente, existe un \'unico isomorfismo \( \psi_2\) entre \( F_2\) y \( F_w\) con las mismas
propiedades. En este punto estamos usando el hecho de que las dos diferenciales tienen los mismos
per\'iodos.

Componiendo los isomorfismos \( \psi_1\) y \( \psi_2^{-1}\), obtenemos un isomorfismo de \( F_1\) a
\( F_2\) que manda la curva \( f_1(\gamma)\) en una curva homot\'opica a \( f_2(\gamma)\).
Para cada franja horizontal construimos dicho isomorfismo. Todos estos isomorfismos son compatibles en los puntos cr\'iticos de la diferencial. Veamos que los isomorfismos tambien son compatibles en
las trayectorias silla.

Sea \( \sigma\) una trayectoria silla que est\'a en la frontera de dos franjas horizontales y que parte de \( p\), un cero de la diferencial. Cada punto \( q\in\sigma\) est\'a a una distancia finita de \( p\) en la m\'etrica de \( \varphi_1\), y esta distancia es invariante bajo isomorfismos de la diferencial cuadr\'atica. Por esta raz\'on, la imagen de \( q\) bajo cada uno de los distintos isomorfismos sobre las dos franjas horizontales que tienen a \( \sigma\) en la frontera, coincide. Esto es cierto para cualquier punto en \( \sigma\), por lo que los isomorfismos son compatibles en \( \sigma\) y esto es cierto para toda trayectoria separatriz.

Con esto hemos demostrado que los isomorfismos definen un homeomorfismo global \( g\) que es isomorfismo complejo en el interior de cada franja. Esto implica que el homeomorfismo de hecho es un biholomorfismo global. Adem\'as, el \( g^*(\varphi_2)= \varphi_1\) en el interior de cualquier franja, y dado que \( g^*(\varphi_2)\) y  \(\varphi_1\) son secciones meromorfas del mismo haz de l\'ineas,
esto basta para demostrar que son iguales.
Con esto concluimos que el biholomorfismo \( g\) es isomorfismo de diferenciales \( \varphi_0-\)marcadas, lo que concluye la demostraci\'on del teorema \ref{teo:principal}.

El teorema \ref{teo:principal} es equivalente a la proposici\'on 4.9 de \cite{bridgeland2015}
y la prueba es una adaptaci\'on de la prueba de dicha proposici\'on.



\subsection{Disgresi\'on: Aplicaciones cuasiconformes y el m\'odulo de un cilindro}

En este secci\'on definimos la noci\'on de \emph{m\'odulo} de un cilindro y
de \emph{aplicaci\'on cuasiconforme}. Adem\'as mencionamos los teoremas que fueron usados en la demostraci\'on del lema \ref{lema:extension-polos}. Por cuestiones de espacio y dado que el tema se escapa
al objetivo del presente texto, omitiremos dar las demostraciones.
La referencia cl\'asica para estos temas es \cite{ahlfors}, en donde se pueden encontrar
todas las demostraciones omitidas. Otra posible referencia es \cite{hubbard2006teichmuller}.

El material de esta secci\'on es bien conocido y es uno de los posibles puntos de partida para el estudio de los \emph{espacios de Teichm\"uller}.

Sea \( A\) una superficie de Riemann que es difeomorfa al disco unitario perforado. A este tipo de superficies les llamaremos \emph{cilindros}.
El cubriente universal de cualquier cilindro es una superficie de Riemann difeomorfa al plano
y el grupo de transformaciones cubrientes (que es isomorfo a \( \mathbb{Z}\)) actua por isomorfismos complejos.
El teorema de uniformizaci\'on implica que el cubriente universal es el plano complejo o el plano hiperb\'olico.
En el primer caso, se puede demostar que el grupo de transformaciones cubrientes actua como

\begin{align*}
	\mathbb{Z}\times \mathbb{C} & \xrightarrow{~~~~~} \mathbb{C}\\
n,z & \mapsto z+2\pi in
\end{align*}

por lo que \( A\) que es el cociente, es isomorfo al plano menos el origen (la aplicacion \( z\mapsto e^z\) induce dicho isomorfismo).

En el caso de que el cubriente sea el plano hiperb\'olico, existen dos opciones:

Si el generador del grupo de transformaciones cubrientes es de tipo parab\'olico, se puede demostrar que la acci\'on del grupo es equivalente a la acci\'on:

\begin{align*}
	\mathbb{Z}\times \mathbb{H} & \xrightarrow{~~~~~} \mathbb{H}\\
n,z &\mapsto z+n
\end{align*}
por lo que en este caso el cociente es isomorfo al cilindro
\( \mathbb{A}_\infty:= \{z\in \mathbb{C} | 1 \leq | z |\}\)

Por otro lado, si el generador es un automorfismo de tipo hiperb\'olico, se puede demostrar que
existe \( r>0\) tal que la acci\'on del grupo es equivalente a la acci\'on:


\begin{align*}
	\mathbb{Z}\times \mathbb{H} & \xrightarrow{~~~~~} \mathbb{H}\\
n,z &\mapsto e^{(nr)}z
\end{align*}
o equivalentemente:
\begin{align*}
	\mathbb{Z}\times \mathbb{B} & \xrightarrow{~~~~~} \mathbb{B}\\
n,z &\mapsto nrz
\end{align*}
donde \( \mathbb{B}:=\{z\in \mathbb{C}| 0 \leq \im(z) \leq \pi\}\) es el modelo de banda del
plano hiperb\'olico.

Usando este \'ultimo modelo, se puede demostrar que la transformaci\'on \(z\mapsto e^{-2\pi i (z/r)} \) induce un isomorfismo entre el cociente y el cilindro
\( \mathbb{A}_{M}:= \{z\in \mathbb{C} | 1 \leq | z | \leq e^{2\pi M}\}\)
donde \( M:=\pi/r\).

Lo anterior constituye un esbozo de clasificaci\'on del tipo de isomorfismo complejo
de todas las superficies de Riemann que sean topologicamente un cilindro.

\begin{definicion}\label{def:modulo}
Al n\'umero \( M\) que aparece en la definici\'on del cilindro \( \mathbb{A}_M\) se le llama
el \emph{m\'odulo} del cilindro y es un invariante complejo. Cuando \( M=\infty\) diremos que
el cilindro es \emph{infinito}. Cuando el cilindro sea biholomorfo al plano complejo perforado, diremos que es doblemente infinito. Es claro que \( \mathbb{D}\setminus \{0\}\) es un cilindro infinito,
es decir, tiene m\'odulo infinito.
Usualmente denotaremos el m\'odulo de un cilindro \( A\) por \( m(A)\).
\end{definicion}

Resumimos las afirmaciones anteriores en el siguiente lema:

\begin{lema}\label{lema:modulo-invariante}
El m\'odulo es el unico invariante holomorfo (o conforme) de las superficies
que topologicamente son un cilindro.
\end{lema}

Para m\'as detalles sobre esta definici\'on y la demostraci\'on del teorema anterior, v\'ease \cite[p\'ag.~11]{ahlfors} o%
%la proposici\'on (3.2.1) de
\cite[prop. 3.2.1]{hubbard2006teichmuller}.

Como ya hemos indicado, el m\'odulo de un cilindro es un invariante conforme, sin embargo,
en ciertas ocasiones la clase de aplicaciones conformes es demasiado restringida como para ser \'util.
Es por esto que se introduce la noci\'on de aplicaci\'on \emph{cuasiconforme}. A continuaci\'on
definiremos de la manera m\'as concisa posible dicha noci\'on.

Sea \( f\) una transformaci\'on \( c^1\) entre dos abiertos del plano complejo y sea \( p \in U\)
cualquier punto en el dominio.

La dilataci\'on (puntual) de \( f\) en \( p\) es el n\'umero
\[D_f(p):=\frac{|f_z|+|f_{\overline z}|}{|f_z|-|f_{\overline z}|}(p) \]

Notemos que la dilataci\'on es una cantidad mayor o igual a uno que es continua en el caso de
que la derivada de \( f\) sea continua, en particular, la dilataci\'on de un difeomorfismo
est\'a acotada en cualquier subconjunto compacto de su dominio.
Tambie\'n notemos que \( D_f(p)=1\)
exactamente cuando \( f_{\overline z}=0\), es decir, cuando \( f\) sea conforme en \( p\).

\begin{definicion}\label{def:casiconforme}
Definimos la dilataci\'on de \( f\) como el supremo de las dilataciones puntuales sobre el dominio de \( f\). Si la dilataci\'on de \( f\) es finita, diremos que \( f\) es una aplicaci\'on cuasiconforme
de constante \( K:=sup(D_f(p))\) o m\'as concisamente que es \( K-\)cuasiconforme.
\end{definicion}

Es claro que una aplicaci\'on \( 1-\)cuasiconforme es de hecho conforme.

La noci\'on de cuasiconformalidad se puede extender a homeomorfismos que posiblememente no sean
diferenciables. Para esto se usa la noci\'on de derivadas distribucionales.
Para evitar entrar en detalles t\'ecnicos, referiremos al lector al cap\'itulo 2 de \cite{ahlfors}
y al cap\'itulo 4 de \cite{hubbard2006teichmuller}.

Como ya mencionamos, una de las virtudes de las aplicaciones cuasiconformes, es que son m\'as abundantes que las aplicaciones conformes.
Otra propiedad \'util es que algunos invariantes conformes dan lugar a \emph{cuasi-invariantes} cuasiconformes.

Uno de los ejemplos m\'as importantes de dichos \emph{cuasi-invariantes} es precisamente el m\'odulo de un cilindro.

\begin{proposicion}[Teorema de Gr\"otzsch para cilindros]
	Sea \( f: A\rightarrow A^\prime\) una aplicaci\'on \( K-\)cuasiconforme entre
	los cilindros de m\'odulos \( m\) y \( m^\prime\).
	Entonces \( m\) y \( m^\prime\) satisfacen:
	\[\frac 1K m^\prime \leq m \leq K m^\prime \]
	y es en este sentido que decimos que el m\'odulo es un \emph{cuasi-invariante} cuasiconforme.
\end{proposicion}

Para la demostraci\'on de esta proposici\'on v\'ease el teorema 1.2 de \cite{ahlfors} o la secci\'on 4.3 de \cite{hubbard2006teichmuller}.

Observemos que por la manera en que se defini\'o la dilataci\'on de un homeomorfismo,
es decir, que depende \'unicamente de cantidades invariantes bajo cambio de coordenadas
holomorfos, la noci\'on de dilataci\'on se puede definir tambi\'en para
homeomorfismos entre superficies de Riemann. Esto implica que la noci\'on de
aplicaci\'on cuasiconforme tambi\'en se puede definir para superficies de Riemann.

Finalmente, la siguiente proposici\'on nos permite  demostrar que una aplicaci\'on
es cuasiconforme demostrando que es cuasiconforme en una colecci\'on de subconjuntos del dominio:

\begin{proposicion}
	Sean \( X\) y \( Y\) dos superficies de Riemann y sea \( f:X\rightarrow Y\) un
	homeomorfismo. Supongamos que existe un subconjunto \( A\) de \( X\) que es compacto
	y que topol\'ogicamente es una gr\'afica encajada. Si  \( f\) es \( K-\)cuasiconforme
	sobre cada componente conexa de \( X\setminus A\), entonces \( f\) es cuasiconforme.
\end{proposicion}

Esta proposici\'on se sigue de:

\begin{proposicion}\label{prop:pegado-casiconforme}
Sea \( f:U \rightarrow V\) un homeomorfismo con \( U\) y \( V\) subconjuntos del plano complejo.
Si \( f\) es cuasiconforme en el
complemento del eje real, entonces \( f\) es cuasiconforme.
\end{proposicion}
La demostraci\'on de esta proposici\'on se sigue la proposici\'on 4.2.7 de \cite{hubbard2006teichmuller}.









