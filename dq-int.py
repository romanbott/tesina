# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.collections import LineCollection
from scipy.integrate import odeint
from cmath import *
########################################
#Definicion de la diferencial cuadratica
def dq(z):
	return (fase*((1/z**4+z-1+1/(z-complex(0.3,0.7))**3)*((z+1.2)**3))).conjugate() #diferencial cuadratica

#parametros

maxreps=9800 #repeticiones maximas de la rutina de integracion
fase=1.0 #fase de la diferencial
normav=0.1 #determina la norma del campo
normamax = 3.0 #determina el area dentro de la cual se integra el campo
t= np.linspace(0,0.015,10) #intervalo temporal
densidadPuntos=5 #determina cada cuantas repeticiones de la rutina de integracion se guarda un punto para su graficacion
colorLineas = 'k'
anchoLineas = 0.2
actualizaClick = 0#determina si se redibuja la figura cada click
dibujaEjes = 0


#inicializacion de la figura
fig = plt.figure()
ax = fig.add_subplot(111, xlim=(-2,2), ylim=(-2,2), autoscale_on=False)
offs= (0.0, 0.0)
ax.get_xaxis().set_visible(dibujaEjes)
ax.get_yaxis().set_visible(dibujaEjes)
coleccionTrayectorias=[]
col = LineCollection(coleccionTrayectorias,offsets=offs,color=colorLineas,linewidths=anchoLineas,antialiaseds=True)
ax.add_collection(col, autolim=True)

#Constantes e inicializacion de variables
ri=sqrt(1j)
ric=sqrt(-1j)
monodromia=0
ma=0
patch1=0
patch2=0

########### funciones #########
def mon(inicio, fin):
	global monodromia
	global ma
	global patch1
	global patch2
	if inicio.real<=0 and inicio.imag>0 and fin.real<=0 and fin.imag<0:
		monodromia=(monodromia + 1)%2
		ma=(ma+1)%2
		print('m')
		print(str(patch1)+str(patch2))
	if inicio.real<=0 and inicio.imag<0 and fin.real<=0 and fin.imag>0:
		monodromia=(monodromia + 1)%2
		ma=(ma+1)%2
		print('-m')
		print(str(patch1)+str(patch2))
	if (-1)*inicio.real>abs(inicio.imag):
		if fin.real<0 and fin.imag>(-1)*fin.real:
			patch1=(patch1+1)%2
			ma=0
		if fin.real<0 and fin.imag<fin.real:
			patch2=(patch2+1)%2
			ma=0
	if inicio.imag<0 and inicio.imag<inicio.real:
		if (-1)*fin.real>abs(fin.imag):
			patch2=(patch2+1)%2
	if inicio.imag>0 and inicio.imag>(-1)*inicio.real:
		if (-1)*fin.real>abs(fin.imag):
			patch1=(patch1+1)%2
	if patch1 and patch2:
		print("igu")
		patch1=0
		patch2=0
	return




def trayectp(z):
	global monodromia
	global patch1
	global patch2
	global ma
	monodromia, patch1, patch2, ma, = 0, 0, 0, 0
	norma=1
	x=[]
	y=[]
	rep=0
	inicio=dq(z)
	fin=z
	if (-inicio.real)>abs(inicio.imag) and inicio.imag>0:
		patch1=1
	if (-inicio.real)>abs(inicio.imag) and inicio.imag<0:
		patch2=1
	inicio = z
	while (10**-10 < abs(dq(fin)) and norma < normamax and rep<maxreps and abs(dq(fin))<10**8):
		sol = odeint(f,[inicio.real, inicio.imag],t)
		fin= complex(sol[-1,0],sol[-1,1])
		mon(dq(inicio),dq(fin))
		inicio = fin
		norma = abs(inicio)
		if rep%densidadPuntos==1:
			x.extend(sol[-1:,0])
			y.extend(sol[-1:,1])
		rep=rep+1
	print('Trayectoria positiva, '+str(rep)+' repticiones')
	return [x,y]
def trayectn(z):
	global monodromia
	global patch1
	global patch2
	global ma
	monodromia, patch1, patch2, ma, = 0, 0, 0, 0
	norma=1
	x=[]
	y=[]
	rep=0
	inicio=dq(z)
	fin=z
	if (-inicio.real)>abs(inicio.imag) and inicio.imag>0:
		patch1=1
	if (-inicio.real)>abs(inicio.imag) and inicio.imag<0:
		patch2=1
	inicio = z
	while (10**-10 < abs(dq(fin)) and norma < normamax and rep<maxreps and abs(dq(fin))<10**8):
		sol = odeint(fi,[inicio.real, inicio.imag],t)
		fin= complex(sol[-1,0],sol[-1,1])
		mon(dq(inicio),dq(fin))
		inicio = fin
		norma = abs(inicio)
		if rep%densidadPuntos==1:
			x.extend(sol[-1:,0])
			y.extend(sol[-1:,1])
		rep=rep+1
	print('Trayectoria negativa, '+str(rep)+' repticiones')
	return [x,y]
def trayectoria(z):
	tn=trayectn(z)
	tp=trayectp(z)
	x=tn[0]
	x.reverse()
	x.extend(tp[0])
	y=tn[1]
	y.reverse()
	y.extend(tp[1])
	return [x,y]
def dist(z):
	if patch1 != patch2:
		if patch1:
			r=ri*(sqrt(z*(-1j)))
		if patch2:
			r=ric*(sqrt(z*(1j)))
	else:
		r=sqrt(z)
	return (-1)**(monodromia+ma)*r
def f(y,t):
	xi=y[0]
	yi=y[1]
	z=dist(dq(complex(xi,yi)))
	if z!=0:
		z=z/abs(z)*normav
	f0 = z.real
	f1 = z.imag
	return [f0, f1]
def fi(y,t):
	xi=y[0]
	yi=y[1]
	z=-dist(dq(complex(xi,yi)))
	if z!=0:
		z=z/abs(z)*normav
	f0 = z.real
	f1 = z.imag
	return [f0, f1]

#interfaz
def onpress(event):
	global fase
	if event.key=='d':
		fase = fase*rect(1,-0.1)
		col.set_segments(trayectorias())
		fig.canvas.draw()
		print('detecto a')
		print(fase)
	if event.key=='a':
		fase = fase*rect(1,0.1)
		col.set_segments(trayectorias())
		fig.canvas.draw()
		print('detecto d')
		print(fase)
	if event.key=='r':
		print('redibujando...')
		col.set_segments(coleccionTrayectorias)
		fig.canvas.draw()
	if event.key=='e':
		plt.savefig('figura.pgf')

def onclick(event):
	global coleccionTrayectorias
	curvaPrueb=trayectoria(complex(event.xdata,event.ydata))
	coleccionTrayectorias.append(list(zip(curvaPrueb[0],curvaPrueb[1])))
	print('Click en la coordenada: '+str(event.xdata)+str(event.ydata)+' en donde la diferencial vale: '+str(dq(complex(event.xdata,event.ydata))))
	if actualizaClick ==1:
		col.set_segments(coleccionTrayectorias)
		fig.canvas.draw()
fig.canvas.mpl_connect('key_press_event', onpress)
fig.canvas.mpl_connect('button_press_event', onclick)
#mostrar el ambiente
plt.show()
