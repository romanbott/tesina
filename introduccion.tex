%! TEX root = master.tex
\chapter*{Introducci\'on}
\addcontentsline{toc}{chapter}{Introducci\'on}

%\blockquote[\cite{hubbard2006teichmuller}]{Why is Teichm\"uller theory significant?
%All areas of mathematics tend to wax and wane, and Teichm\"uller theory in particular has gone through multiple cycles of popularity and unpopularity. There have been times when some (many?) mathematicians looked down on 1-dimensional complex analysis and on low-dimensional topology as special cases that are unrepresentative of general phenomena and unworthy of serious attention.}


En este trabajo se intenta presentar de manera concisa pero completa la teor\'ia y construcciones necesarias
para construir familias de diferenciales cuadr\'aticas (y parametrizaciones naturales de estas)
con caracter\'isticas muy particulares.

El objetivo del presente texto surgi\'o de los intentos de comprender el art\'iculo
\cite{bridgeland2015}. Dada la extensi\'on y profundidad de este art\'iculo, se escogi\'o
trabajar sobre un fragmento del mismo y adem\'as restringirse a un caso particular:
el de las diferenciales GMN con polos dobles y sin trayectorias silla.

A continuaci\'on se presenta un breve panorama tanto de \cite{bridgeland2015}, como 
del alcance del presente trabajo y su relaci\'on con el art\'iculo.

Una \emph{diferencial cuadr\'atica} sobre una superficie de Riemann es una secci\'on meromorfa del
cuadrado tensorial del haz can\'onico de la superficie. Localmente se puede expresar como
\( f(z)dz^2\) en donde \( z\) es una coordenada holomorfa y \( dz^2\) es una abreviaci\'on de \( dz\otimes dz\).

Las diferenciales cuadr\'aticas, que a priori son objeto de estudio de la geometr\'ia compleja,
tienen asociadas estructuras geom\'etricas reales. Cada diferencial cuadr\'atica da lugar
a una m\'etrica con singularidades c\'onicas sobre la superficie y a una familia de foliaciones por geod\'esicas
parametrizada por el c\'irculo.
Ambas estructuras son fundamentales para el estudio posterior de las diferenciales cuadr\'aticas
y son el objeto de estudio del cap\'itulo \ref{cap:geometria-diferenciales}.

Uno de los resultados fundamentales concernientes a estas estructuras geom\'etricas es el hecho
de que todas son localmente triviales salvo en una cantidad finita de puntos:
la m\'etrica y las foliaciones son isomorfas a la m\'etrica euclidiana en \( \mathbb{C}\) y a las
foliaciones por rectas paralelas. Esto es equivalente a que siempre se pueden encontrar cartas en las que la diferencial cuadr\'atica
se expresa como \( dz^2\).

Del c\'irculo de foliaciones asociado a una diferencial cuadr\'atica, existe una distinguida: la foliaci\'on horizontal.
Esta corresponde a la foliaci\'on por rectas horizontales en el plano complejo. A las hojas de dicha foliaci\'on
se le llaman trayectorias (horizontales) de la diferencial cuadr\'atica.
La estructura global de las trayectorias horizontales de una diferencial cuadr\'atica puede llegar a ser muy complicada
y es de uno de los temas centrales en la teor\'ia.

Retomando el punto de vista de la geometr\'ia compleja,
las diferenciales cuadr\'aticas son el an\'alogo <<cuadr\'atico>> de las \( 1-\)formas diferenciales meromorfas y al igual que estas, se pueden integrar a lo largo de curvas,
pero en este caso es necesario primero tomar la ra\'iz cuadrada de la diferencial cuadr\'atica.
Un aspecto ligeramente t\'ecnico es que la ra\'iz cuadrada de la diferencial cuadr\'atica no est\'a
bien definida globalmente sobre la superficie original sino en un cubriente de dos hojas de esta.
A dicho cubriente se le llama \emph{cubierta espectral}. Desde este punto de vista,
la diferencial cuadr\'atica da lugar a un homomorfismo del grupo de homolog\'ia de la cubierta espectral
en los n\'umeros complejos. Este homomorfismo es el an\'alogo de los periodos de una \( 1-\)forma meromorfa.
Discutiremos la construcci\'on de la cubierta espectral y sus propiedades en el cap\'itulo \ref{cap:espectral-hom}.

Las diferenciales cuadr\'aticas se han estudiado por lo menos desde los a\~nos treinta del siglo XX\footnote{V\'ease \cite[p.~VI]{strebel-quadratic}} y aunque surgieron en el contexto del an\'alisis funcional y variacional, actualmente son relevantes para una gran variedad de \'areas.

La primera gran aplicaci\'on fue en el trabajo de Oscar Teichm\"uller con relaci\'on a transformaciones cuasiconformes, lo que eventualmente llev\'o a la construcci\'on de los espacios de Teichm\"uller y a una construcci\'on anal\'itica del espacio modular de superficies de Riemann (que a grandes razgos es la manera correcta de <<geometrizar>> el conjunto de clases de isomorfismo de superficies de Riemann).

Posteriormente, hacia la segunda mitad del siglo XX, se comienza a estudiar
la topolog\'ia y geometr\'ia de los espacios modulares de diferenciales cuadr\'aticas,%
\footnote{Y tambi\'en de diferenciales abelianas o \( 1-\)formas meromorfas,
mismos que est\'an intimamente relacionados con los espacios modulares de diferenciales cuadr\'aticas.}
es decir, espacios cuyos puntos est\'an en biyecci\'on con las clases de isomorfismo
de diferenciales cuad\'raticas. Usualmente se impone alg\'un tipo de restricci\'on a los polos
de las diferenciales.

Por lo general, estos espacios tienen una estructura geom\'etrica muy complicada.
Usualmente son espacios singulares (orbifolds) estratificados y cuyos estratos son disconexos.
Adem\'as estos espacios poseen propiedades din\'amicas: una acci\'on del grupo \(\GeneralLinear_+(2,\mathbb{R}) \)
y un flujo que recibe el nombre de \emph{flujo de Teichm\"uller}, as\'i como una medida
invariante bajo dicho flujo.%
\footnote{V\'eanse, por ejemplo, los trabajos de Kontsevich-Zorich y Lanneau sobre el n\'umero de componentes conexas \cite{kontsevich2003,lanneau2008}; los trabajos de Eskin-Okounkov sobre el volumen del espacio modular \cite{eskin2001}; y el trabajo de Avila-Viana sobre la conjetura Kontsevich-Zorich \cite{avila2005}, entre muchos otros trabajos sobre estos espacios.}


Entre los aspectos m\'as recientemente estudiados de esta teor\'ia, se encuentran los concernientes a su relaci\'on con \emph{condiciones de estabilidad} sobre \emph{categor\'ias trianguladas}.%
\footnote{Para una muy breve introducci\'on a las cateogr\'ias derivadas y trianguladas, v\'ease \cite{thomas2000derived}.}

Si \( \sigma\) es una condici\'on de estabilidad sobre una categoria \( \mathcal{C}\),
esta da lugar a un morfismo \(Z_\sigma: K(\mathcal{C})\rightarrow \mathbb{C}\) al que se le llama la carga centra,
donde \( K(\mathcal{C})\) denota al \emph{grupo de Grothendieck} de la categor\'ia.
El conjunto de condiciones de estabilidad sobre \( \mathcal{C}\) se denota por \( Stab(\mathcal{C})\) y
en \cite{bridgeland2007} se demuestra que posee una topolog\'ia natural con la cual es una variedad compleja
y que la aplicaci\'on que a cada condici\'on de estabilidad le asigna su carga central es un isomorfismo local.

Desde el a\~no 2007, Maxim Kontsevich ha hecho las siguientes observaciones:
\blockquote[\cite{kontsevich2007donaldson}]{
Hence, $Stab(\mathcal{C})$ is a complex manifold, not necessarily connected.
Under our assumptions one can show that the group $Aut(\mathcal{C})$ acts properly discontinously on $Stab(\mathcal{C})$.
On the quotient orbifold $Stab(\mathcal{C})/Aut(\mathcal{C})$ there is a natural non-holomophic action of $\GeneralLinear_+(2,\mathbb{R})$ arising from linear transformations of $\mathbb{R}^2\cong \mathbb{C}$ preserving the standard orientation. A similar geometric structure appears on the moduli spaces of holomorphic Abelian differentials,\textelp{}}

El siguiente a\~no, en un art\'iculo de Kontsevich y Yan Soibelman, encontramos la siguiente observaci\'on:

\blockquote[\cite{kontsevich2008stability}]{
Geometry similar to the one discussed in this paper also appears in the theory of moduli spaces of holomorphic abelian differentials \textelp{} 
The moduli space of abelian differentials is a complex manifold, divided by real “walls” of codimension one into pieces glued from convex cones. It also carries a natural non-holomorphic action of the group $\GeneralLinear_+(2,\mathbb{R})$. There is an analog of the central charge $Z$ in the story. It is given by the integral of an abelian differential over a path between marked points in a complex curve. This makes plausible the idea that the moduli space of abelian differentials associated with a complex curve with marked points, is isomorphic to the moduli space of stability structures on the (properly defined) Fukaya category of this curve.}

Desde hace algunos a\~nos, las diferenciales cuadr\'aticas aparecieron en un campo
sin una aparente conexi\'on: el estudio de soluciones asint\'oticas de ecuaciones diferenciales
parciales, m\'as espec\'ificamente, el m\'etodo WKB.%
\footnote{El m\'etodo Wentzel–Kramers–Brillouin surgi\'o originalmente en mec\'anica cu\'antica como un m\'etodo
para obtener soluciones aproximadas de la ecuaci\'on de Schr\"odinger.}
En este contexto, las trayectorias horizontales que emanan de un cero de la diferencial cuadr\'atica
reciben el nombre de \emph{curvas de Stokes} y su estudio se origin\'o del llamado
\emph{fen\'omeno de Stokes}.
En \cite{kawai} se introduce la idea de que bajo ciertas condiciones, la \emph{gr\'afica de Stokes}
da lugar a una triangulaci\'on de la esfera de Riemann.

A\~nos m\'as tarde, en \cite{neitzke-wall-crossing} se expone la idea de que bajo ciertas hip\'otesis, toda diferencial cuadr\'atica (meromorfa) define una triangulaci\'on ideal, cuyos vertices son ex\'actamente los polos de orden dos de la diferencial cuadr\'atica y cuyos arcos son ciertas trayectorias horizontales. A dicha triangulaci\'on se le llama la \emph{triangulaci\'on WKB}.
As\'i mismo, se especula sobre una posible conexi\'on entre los espacios de diferenciales cuadr\'aticas y
la f\'ormula de \emph{wall-crossing} de Kontsevich y Soibelman.

Por otro lado, desde hace algunos a\~nos, se conoce el hecho de que toda triangulaci\'on de una superficie da lugar 
a una categor\'ia triangulada.
Para una visi\'on panor\'amica de los pasos necesarios para construir dicha categor\'ia ve\'ase \cite{2013labardini}.

Finalmente, sintetizando los resultados ya mencionados, en \cite{bridgeland2015} se demuestra, \textit{grosso modo}, que el espacio de las diferenciales cuadr\'aticas sobre una superficie de Riemann es isomorfo al espacio de condiciones de estabilidad sobre la categor\'ia triangulada de cualquier triangulaci\'on determinada por cualquier diferencial cuadr\'atica sobre la superficie de Riemann.
Uno de los puntos m\'as delicados del art\'iculo es demostrar la invarianza de la categor\'ia triangulada bajo perturbaciones de la diferencial cuadr\'atica.

En este trabajo se exponen las herramientas necesarias para construir la \emph{triangulaci\'on WKB}
a partir de una diferencial GMN sin trayectorias silla.
As\'i mismo, se presenta el material necesario para demostrar que el conjunto
de diferenciales cuadr\'aticas GMN sin polos de orden mayor a 2 y sin trayectorias silla
se descompone naturalmente como una union de familias parametrizadas por \( \mathbb{H}^n\).
Esto \'ultimo corresponde aproximadamente a la secci\'on \( 4.5\) de \cite{bridgeland2015}.

Desde un punto de vista topol\'ogico, estas familias corresponden a celdas con complemento de
codimensi\'on \( 1\), es decir, dan lugar a una descomposici\'on en \emph{c\'amaras y muros} del espacio de diferenciales cuadr\'aticas.

No es dif\'icil ver que en cada una de dichas celdas, la triangulaci\'on WKB se mantiene constante,
por lo que demostrar la invarianza de la categoria triangulada al variar la diferencial cuadr\'atica
se traduce en realizar un an\'alisis detallado del comportamiento de la triangulaci\'on al pasar de
una celda a otra. Esto es precisamente el argumento central de \cite{bridgeland2015}.


%cubiertas espectrales y el mapeo de periodos
