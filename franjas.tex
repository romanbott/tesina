\chapter{Franjas horizontales y periodos}

En este cap\'itulo nos enfocaremos en el estudio de diferencial cuadr\'aticas en cuya
descomposici\'on solo aparecen franjas horizontales y semiplanos.
Construiremos una base natural para la homolog\'ia gorro de dichas diferenciales y
describiremos expl\'icitamente el conjunto de clases de isomorfismo de las franjas horizontales.

\section{Franjas horizontales y clases silla}

El objetivo de esta secci\'on es encontrar una clase de modelos estandar para
las franjas horizontales.

\subsection{Trayectorias}

Hasta este momento, en el estudio de las diferenciales cuadr\'aticas,
un\'icamente hemos mencionado la foliaci\'on y las trayectorias horizontales, sin embargo,
las diferenciales cuadr\'aticas tienen asociadas toda una familia de foliaciones parametrizada por \( \mathbb{S}^1\cong \mathbb{R} /\mathbb{Z}\).

Para ver esto, notemos que aquellas estructuras que se preservan bajo cambios de coordenadas normales,
son intr\'insicas a la diferencial cuadr\'atica. De esta forma fue como se defini\'o la m\'etrica y la foliaci\'on horizontal.
As\'i mismo, localmente dichas estructuras corresponden a las estructuras de \( \mathbb{C}\) que se preservan bajo traslaciones
y la involuci\'on \( z\mapsto -z\).

A la foliaci\'on de \( \mathbb{C}\) cuyas hojas son las todas las rectas que forman el \'angulo \( \pi \theta\) con el eje
horizontal le llamaremos la \emph{foliaci\'on constante de fase \( \theta\)} de \( \mathbb{C}\).

Es claro que todas las  foliaci\'on constantes en \( \mathbb{C}\) se preservan bajo las transformaciones antes mencionadas,
por lo que es de esperar que se pueda definir el concepto de foliaci\'on de fase \( \theta \) asociada a cualquier diferencial cuadr\'atica.

De manera totalmente an\'aloga a la definici\'on \ref{def:metrica-foliacion} tenemos:

\begin{definicion}
La \emph{foliaci\'on de fase \( \theta\)} determinada por \( \varphi\) es la \'unica foliaci\'on que en cada carta normal coincide
con la foliaci\'on constante  de fase \( \theta\) en \( \mathbb{C}\).

A las hojas de dicha foliaci\'on les llamaremos \emph{trayectorias de fase \( \theta\)}. As\'i mismo, cuando hablemos de una trayectoria de la diferencial, nos referiremos a una trayectoria de cualquier fase.
\end{definicion}

Es claro que dichas trayectorias se pueden caracterizar de varias maneras:

\begin{proposicion}
Sea \( \varphi\) una diferencial cuadr\'atica y \( \theta\in\mathbb{R}/\mathbb{Z}\).
Una curva \( \gamma\) es una trayectoria de fase \( \theta\) si y solo si cumple cualquiera de las siguientes propiedades:
\begin{itemize}
 \item la funci\'on \[ q\mapsto \im\left(e^{-i\pi\theta}\int_p^q\sqrt{\varphi}\right)\]
(que est\'a definida localmente) es constante a lo largo de \( \gamma\)
\item es una trayectoria horizontal de la diferencial cuadr\'atica \( e^{-2\pi i \theta}\varphi\)
\end{itemize}

\end{proposicion}
\begin{proof}
	Como ser trayectoria de fase \( \theta\) y todas las propiedades mencionadas en la proposici\'on son locales,
	basta trabajar en una carta normal. Sea \( w\) la funci\'on dada por \( w(q):= \int_p^q\sqrt{\varphi}\). 
	\( w\) es una carta normal y en t\'erminos de \( w\) la funci\'on del primer inciso est\'a dada por
	\( \im(e^{-i\pi\theta}w(p))\) que es claro que es constante en las hojas de la foliaci\'on de fase \( \theta\) en la carta \( w\).
	Esto demuestra la equivalencia con la primera propiedad.

	Para ver la equivalencia con la segunda propiedad, notemos que la transformaci\'on \( \Phi(z):=e^{-i\pi\theta}z\)
	lleva las rectas de fase \( \theta\) en las rectas horizontales y adem\'as satisface que \( \Phi^*(dz^2)=e^{-2i\pi\theta} dz^2\)
	por lo que las rectas horizontales de esta \'ultima diferencial son exactamente las rectas de fase \( \theta\).
	Localmente, esto es la equivalencia con la segunda propiedad.
\end{proof}

\begin{observacion}
	Es claro que toda trayectoria de una diferencial cuadr\'atica es una geod\'esica de la m\'etrica.
	As\'i mismo, las diferenciales cuadr\'aticas \( \varphi\) y \( e^{2\pi i \theta}\varphi\) definen la misma m\'etrica.
	Por otro lado, se puede demostrar que toda geod\'esica de la diferencial cuadr\'atica es una uni\'on de trayectorias
	de fases posiblemente distintas.
	 \end{observacion}

Las \emph{conexiones silla} son una tipo particular de trayectoria que jugar\'an un papel fundamental m\'as adelante:

\begin{definicion}
	Una \emph{conexi\'on silla} de una diferencial cuadr\'atica \( \varphi\)
	es una trayectoria de alguna fase \( \theta\) que conecta dos puntos cr\'iticos finitos de \( \varphi\).
	Equivalentemente, es una trayectoria silla de la diferencial \( e^{-2\pi i \theta}\varphi\).
\end{definicion}

\subsection{Franjas horizontales}

Recordemos que los teoremas \ref{teo:descomposicion} y \ref{teo:triangulacion-wkb} implican
que para una clase de diferenciales cuadr\'aticas, las regiones que aparecen en su descomposici\'on
son todas franjas horizontales, es decir, regiones isomorfas a conjuntos de la forma:
\[ F_{2h}:=\{z\in \mathbb{C}| -h\leq\im(z)\leq h\}\]

%\attention formas de franjas ya encajadas

Ahora bien, el isomorfismo entre una de estas regiones y el conjunto \( F_{2h}\) dista mucho de ser \'unico.
Para ver esto, basta calcular el grupo de automorfismos de \( (F_{2h}, dz^2)\).
Como \( F_{2h}\) es subconjunto de \( \mathbb{C}\) y estamos considerando la diferencial \( dz^2\),
los automorfismos necesariamente son de la forma \( z\mapsto \pm z +b\).
Para que cualquiera de estas transformaciones preserve la franja \( F_{2h}\) debe preservar,
o posiblemente permutar las rectas frontera. Esto implica que \( \im(b)=0\), es decir,
el grupo de automorfismos de \( F_{2h}\) es el grupo de transformaciones \( z\mapsto \pm z +r\) con \( r\) real.

En ciertos casos, es posible reducir la cantidad de isomorfismos entre las franjas horizontales y los conjuntos \( F_{2h}\)
de la siguiente forma.
Supongamos que la franja horizontal en cuesti\'on tiene \'unicamente un cero de la diferencial en cada
componente de la frontera y escojamos cualquier isomorfismo \( \psi\) con un conjunto \( F_{2h}\).
Entonces la imagen de un cero tiene parte imaginaria igual a \( h\) y la imagen del otro cero tiene parte imaginaria \( -h\).
Componiendo \( \psi\) con una traslacion horizontal (que es automorfismo de \( F_{2h}\)) podemos garantizar que la imagen
de los puntos cr\'iticos finitos es sim\'etrica con respecto al origen. 

Consideremos ahora el grupo de automorfismos de un conjunto \( F_{2h}\)
con un par de puntos marcados en la frontera y sim\'etricos con respecto al origen, digamos \( \{w,-w\}\).
Es claro que en este caso, el \'unico automorfismo no trivial es la involuci\'on \( z\mapsto -z\).

A partir de esto, definimos el siguiente concepto:

\begin{definicion}
	La \emph{franja horizontal de periodo \( w\)} es el conjunto
	\[ F_w:=F_{\im(w)}=\{z\in\mathbb{C}| |\im(z)|\leq \frac12\im(w)\}\]
	con los puntos marcados \( \pm \frac12 w\).
	A la \'unica recta que pasa por los puntos marcados se le llama
	la \emph{conexi\'on silla estandar}. Esta es una conexi\'on silla de fase \( \arg(w)\).
	Es claro que \( F_w\) es isomorfo a \( F_{w^\prime}\) si y solo si \( w=\pm w^\prime\).

\end{definicion}

La discusi\'on anterior implica inmediatamente la siguiente proposici\'on:

\begin{proposicion}
	Sea \( \varphi\) una diferencial cuadr\'atica GMN sin trayectorias silla y sin polos de orden mayor a \( 2\).
	En la descomposici\'on de \( \varphi\) \'unicamente aparecen franjas horizontales en cuya frontera hay
	a lo m\'as dos puntos cr\'iticos finitos.

	Cada una de estas franjas horizontales es isomorfa a exactamente una franja horizontal de alg\'un periodo \( w\)
	y adem\'as, la preimagen de la conexi\'on silla estandar de \( F_w\) es la \'unica conexi\'on silla de \( \varphi\)
	que conecta los puntos cr\'iticos finitos de la franja horizontal y que est\'a contenida en esta.

	Diremos que dicha conexi\'on silla es la \emph{conexi\'on silla estandar} de la franja en la que est\'a contenida.
\end{proposicion}

Antes de finalizar esta secci\'on, observamos que el isomorfismo entre una franja horizontal estandar y una franja en la
superficie no necesariamente se extiende a la frontera. De hecho, hay dos posibles configuraciones para la cerradura de una franja horizontal. En las figuras \ref{fig:franja} y \ref{fig:franja-doblada} mostramos ambas posibilidades as\'i como las correspondientes conexiones silla.
\tikzexternalenable
\begin{figure}
\makebox[\textwidth][c]{
\begin{minipage}{0.4\textwidth}
\tikzsetnextfilename{franja}
\hspace{-1.0cm}
\input{fig/franja_silla3_chica.pgf}
\caption{Franja horizontal con dos puntos cr\'iticos finitos en la frontera (en azul).
Tambi\'en se muestran algunas conexiones silla en rojo.} \label{fig:franja}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}{0.4\textwidth}
\hspace{-1.0cm}
\tikzsetnextfilename{franja_doblada}
\input{fig/franja_doblada2_silla_chica.pgf}
\caption{Franja horizontal con un \'unico punto cr\'itico finito en la frontera.
La conexi\'on silla en este caso es un lazo.} \label{fig:franja-doblada}
\end{minipage}
}
\end{figure}

\tikzexternaldisable

\section{Periodos de una diferencial GMN}

En esta secci\'on utilizaremos las construcciones anteriores para dar una descripci\'on de la
homolog\'ia gorro de una diferencial cuadr\'atica.

Por el resto del cap\'itulo, fijemos una superficie de Riemann \( X\) y una diferencial GMN
\( \varphi\) sobre \( X\) sin polos de orden mayor que \( 2\) y
que no tiene trayectorias silla. Como ya hemos mencionado, esto implica
que \( X\) se descompone en una uni\'on de franjas horizontales, cada una de las cuales tiene a lo m\'as dos puntos cr\'iticos finitos en su frontera.

Sea \( F\) cualquier franja horizontal de periodo \( w \) de \( X\).
Sea \( \alpha\) la conexi\'on silla que conecta los puntos cr\'iticos finitos de la frontera de \( F\).
Escojamos cualquier orientaci\'on de \( \alpha\), y consideremos a \( \alpha\) con dicha orientaci\'on
como una \( 1-\)cadena sobre \( X\). Sean \( \alpha_1\) y \( \alpha_2\) los dos levantamientos
de \( \alpha\) al cubriente espectral de \( X\). 
Entonces es claro que \( \alpha_1-\alpha_2\) y \( \alpha_2-\alpha_1\) son \( 1-\)ciclos
que definen clases de homolog\'ia anti-invarianes, es decir, son elementos de \(\widehat\H(\varphi) \),
por lo que tienen periodos bien definidos.

Para calcular el periodo, notemos que se puede calcular como la integral de \( \psi\) a lo largo de cada sumando del \( 1-\)ciclo,
y esto a su vez se puede calcular en la franja horizontal estandar en \( \mathbb{C}\):
\[ \widehat{\varphi}(\pm(\alpha_1-\alpha_2))=\pm\frac12\left(\int_{\alpha_1}\psi-\int_{ \alpha_2 }\psi\right)=\pm\left( \int_{-\frac12 w}^{\frac12 w}dz  \right)=\pm w\]
en donde usamos que \( \int_{\alpha_2}-\psi=\int_{\alpha_2}\tau^*(\psi)=\int_{\tau_*(\alpha_2)}\psi=\int_{\alpha_1}\psi\).

Sea \( \alpha_F\) la \'unica clase de homolog\'ia gorro con representante \( \pm(\alpha_1-\alpha_2)\) y cuyo periodo
tiene parte imaginaria positiva. A \( \alpha_F\) le llamaremos la \emph{clase silla estandar} asciada a la franja \( F\).
Esto \'ultimo explica la nomenclatura escogida: la clase silla estandar de una franja de periodo \( w\) tiene periodo \( w\).

Las clases silla estandar son de gran importancia pues forman una base para la homolog\'ia gorro de la diferencial cuadr\'atica.
Comencemos demostrando que son linealmente independientes:

\begin{proposicion}
	Sean \( F_1, F_2,\ldots,F_n\) las franjas horizontales de la diferencial \( \varphi\) y
	sean \( \alpha_i:=\alpha_{F_i}\) las correspondientes clases silla estandar.
	Las \( \alpha_i\) son linealmente independientes.
\end{proposicion}
\begin{proof}
Sea \( \gamma_i\) cualquier trayectoria gen\'erica de la \( i-\)\'esima franja horizontal.
Considerando cualquier orientaci\'on de \( \gamma_i\) y tomando la diferencia de los dos levantamientos a la cubierta espectral \( \widehat X\) podemos construir una clase en el grupo de homolog\'ia relativo \( \H_1(\widehat X, \CritInf)\) que denotaremos \( [\gamma_i]\).
Usando el apareamiento bilineal discutido al final del cap\'itulo anterior, es claro que
\( \langle \gamma_i, \alpha_j\rangle\) es distinto de cero si y solo si \( i=j\).
Esto implica la independencia lineal de las clases \( \alpha_i\).

\end{proof}

Para ver que las \( \alpha_i\) son base, basta ver que hay el n\'umero correcto de estas.
Recordemos que por la proposici\'on \ref{prop:dimension-h-gorro} la dimensi\'on de \( \widehat{H}(\varphi)\)
es exactamente \( N(\varphi)=6g -6 + \sum_{i=0}^m m_i+m\).

Referimos al lector al lema \( 3.2\) de \cite{bridgeland2015} para la demostraci\'on de que hay exactamente \( N(\varphi)\) franjas
horizontales. Juntando esto con la proposici\'on anterior, obtenemos:
\begin{corolario} Sea \( \varphi\) una diferencial cuadr\'atica GMN sin polos de orden mayor a \( 2\) y sin trayectorias silla.
	Entonces las clases silla estandar \( \alpha_i\) forman una base para el grupo de homolog\'ia gorro \( \widehat\H(\varphi)\).
\end{corolario}


\endinput
Demostraremos que las \( \alpha_i\) son base probando que hay exactamente \( N(\varphi)=6g -6 + \sum_{i=0}^m m_i+m\)%
\footnote{Recordemos que por la proposici\'on \ref{prop:dimension-h-gorro} \( N(\varphi)\) es la dimensi\'on de
	\( \widehat{H}(\varphi)\).}
franjas horizontales y por lo tanto \( N\) clases silla estandar.

\begin{proposicion}
Hay exactamente \( N(\varphi)\) franjas horizontales.
\end{proposicion}
\begin{proof}

\end{proof}







conteo de franjas

orientacion transversal de una separatriz


hay mismo numero de franjas que el rango de la homologia sombrero.

las clases silla son base para la homologia sombrero
