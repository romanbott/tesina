\chapter{Cubiertas espectrales, homolog\'ia y periodos}%
\label{cap:espectral-hom}
En este cap\'itulo atacamos el problema de describir de manera <<homol\'ogica>> una diferencial
cuadr\'atica. En cierto modo, esto es an\'alogo a la descripci\'on de diferenciales abelianas
mediante sus per\'iodos, es decir, la integral de la diferencial sobre una base de la homolog\'ia
de la superficie.
Para esto, el primer paso ser\'a una construcci\'on necesaria para poder
obtener la <<ra\'iz cuadrada>> de una diferencial cuadr\'atica.
Como es usual, dicho procedimiento requiere tomar un cubriente ramificado de dos hojas
sobre la superficie original. Dicho cubriente recibe el nombre de \emph{cubierta espectral}
y daremos los detalles sobre su construcci\'on en la secci\'on \ref{sec:cubierta-espectral}.

De este modo, la <<ra\'iz cuadrada>> de una diferencial cuadr\'atica es naturalmente
una \( 1-\)forma meromorfa sobre la cubierta espectral, y obtendremos los per\'iodos
al integrar dicha forma sobre elementos de la homolog\'ia de la cubierta%
\footnote{M\'as espec\'ificamente, sobre la \emph{homolog\'ia gorro}, que es una subgrupo de la homolog\'ia
de la cubierta.}
por lo que ser\'a necesario conocer el g\'enero de la cubierta y tambi\'en la estructura del grupo de homolog\'ia
de la cubierta. Las herramientas necesarias para esto se desarrollan en la primera secci\'on, mientras que
el estudio detallado de la homolog\'ia de la cubierta se presenta en la \'ultima secci\'on.

Los resultados y definiciones de este cap\'itulo, son en su mayoria,
tomados de \cite{bridgeland2015}, exeptuando los de la primera secci\'on,
que son resultados cl\'asicos en la teor\'ia de superficies de Riemann y
haces de l\'ineas y se pueden consultar, por ejemplo, en \cite{donaldson2011riemann} o
\cite{narasimhan1992compact}.

\section{Existencia de diferenciales cuadr\'aticas}

En el cap\'itulo anterior desarrollamos un entendimiento parcialmente satisfactorio
de la geometr\'ia de una diferencial cuadr\'atica. Para esto usamos 
una caracterizaci\'on local de la geometr\'ia de la diferencial
junto con algunos argumentos globales, lo que eventualmente nos llev\'o al teorema
\ref{teo:descomposicion}.
Para poder profundizar y ampliar dichos resultados, precisaremos un lenguaje
que nos permita hablar de familias de diferenciales cuadr\'aticas.
En particular, tambi\'en estableceremos la existencia de diferenciales cuadr\'aticas
sobre cualquier superficie de Riemann.

%entre otras cosas, veremos que bajo ciertas condiciones ..
%podemos garantizar la existencia de diferenciales cuadr\'aticas

%mas apropiado para hablar de muchas diferenciales simultaneamente.

%En la siguiente subsecci\'on algunos conceptos que necesiaremos para 



\subsection{Disgresi\'on: divisores, el teorema de Riemann-Roch y el grupo de Picard}

Una manera de hablar sistem\'aticamente de ceros y polos de una secci\'on holomorfa es
mediante el uso de divisores:

\begin{definicion}
	Un \emph{divisor} \( D\) sobre \( X\) es una suma formal finita de puntos de \( \mathcal{S}\).
	Es decir, \[ D=\sum n_p p\]
	donde \( n_p\) es un entero, y aparece \'unicamente una cantidad finita de t\'erminos en la suma.
	El \emph{grado} de un divisor \( D=\sum n_p p\) se define como \( \grad(D)= \sum n_p \in \mathbb{Z}\).
\end{definicion} 

	Es claro que los divisores forman un grupo abeliano bajo la adici\'on puntual.
	Dicho grupo se denota \( \Divisor(X)\). El grado es un morfismo de grupos
	\[ \grad: \Divisor(X)\rightarrow\mathbb{Z}.\]

Si \(\sigma\) es una secci\'on meromorfa de un haz de l\'ineas \( L\) con ceros \( p_1,\ldots,p_n\)
y polos \( q_1,\ldots,q_m\), entonces al divisor:
\[ d(\sigma):= \sum_{i=1}^{n} \ord_{p_i}(\sigma)p_i + \sum_{i=1}^{m} \ord_{q_i}(\sigma)q_i \]
donde \( \ord\) denota el orden de \( \sigma\) en un cero o polo, se le llama el \emph{divisor de} \( \sigma\).
A \( \grad(d(\sigma))\) se le llama el grado de \( \sigma\).

Se puede definir un orden parcial en el grupo de divisores de tal forma que
\( D=\sum n_p p\geq D^\prime=\sum m_p p\) si y s\'olo si \( n_p\geq m_p\) para todo \( p\in X\).

Con esta notaci\'on, por ejemplo, decir que \( d(f)\geq p-q\) es equivalente
a que \( f\) tiene por lo menos un cero simple en \( p\) y a lo m\'as un polo simple en \( q\).

En lo sucesivo, denotaremos la gavilla de funciones holomorfas sobre una superficie de Riemann \( X\)
como \( \mathcal{O}_X\). Si la superfice es fija, o se puede deducir del contexto, simplificaremos la notaci\'on
y usaremos \( \mathcal{O}\) para denotar dicha gavilla.

Si \( D\) es un divisor y \(L \) es un haz de l\'ineas podemos definir una gavilla de
\(\mathcal{O} \)-m\'odulos de la siguiente forma:
\[ \mathcal{L}_D (\mathcal{U}):=\left\{\,\sigma~ \middle| \, \sigma:\mathcal{U}\rightarrow L \, \textrm{es una secci\'on meromorfa y } d(\sigma)\geq D \vphantom{\frac12}\right\}.\]
Cuando \( D=0\) denotaremos \( \mathcal{L}_D\) simplemente por \( \mathcal{L}\).
As\'i mismo, a los grupos de cohomolog\'ia de la gavilla \( \mathcal{L}_D\)
los denotaremos como \( H^i(L_D)\).

Recordemos que \( H^0(L)\) es can\'onicamente isomorfo al espacio de secciones globales de \( L\)
y que los grupos de cohomolog\'ia superiores se pueden definir expl\'icitamente
mediante la \emph{cohomolog\'ia de \v{C}ech}, o bien, de manera abstracta como
\emph{funtores derivados} del funtor de secciones globales.

Uno de los resultados m\'as importantes concernientes a los grupos de cohomolog\'ia de haces de l\'ineas es el
llamado \emph{teorema de finitud}:

\begin{teorema}[de finitud]\label{teo:finitud} 
	Sea \( L\) un haz de l\'ineas sobre \( X\).
	Los grupos de cohomolog\'ia de \( L\) satisfacen:
	\[ H^i(L)=0 \quad\textrm{ si }\quad i>1\]
	\[\dim_\mathbb{C}H^0(L)<\infty \text{~~ y~~ }\dim_\mathbb{C}H^1(L)<\infty.\]
\end{teorema}

Con este teorema podemos deducir f\'acilmente la existencia de secciones
meromorfas para cualquier haz de l\'ineas:
\begin{corolario}
	Sea \( L\) un haz de l\'ineas sobre \( X\) y \( p\in X\).
	Existe una secci\'on meromorfa \( \sigma\) de \( L\) que es
	holomorfa en \( X\setminus \{p\}\).
\end{corolario}
%\begin{proof}
	%Sea \( U=X\setminus \{p\}\) y \( V\) un disco conforme centrado en \( p\)
	%en el que \( L\) es trivial. Podemos usar la cubierta \( \mathcal{U}=\{U,V\}\)
	%para calcular la cohomolog\'ia de \v{C}ech de \( \mathcal{L}\).
	%Como \( H^1(\mathcal{L},\mathcal{U})\) se encaja en \( H^1(L)\) y este tiene dimensi\'on
	%finita, concluimos que \( H^1(\mathcal{L},\mathcal{U})\) tambi\'en es de dimensi\'on finita.
	%Mediante una trivializaci\'on de \( L\) sobre \( V\) podemos identificar las secciones
	%holomorfas de \( L\) sobre \( V\) con funciones holomorfas en una vecindad del cero,
	%donde adem\'as, \( p\) corresponde al \( 0\).

%\end{proof}

Sea \( L\) un haz de l\'ineas y consideremos el divisor \( d(\sigma)\) de
cualquiera de sus secciones meromorfas \( \sigma\). Por el corolario anterior,
existe por lo menos una secci\'on y es posible definir dicho divisor.
Ahora bien, si \( \sigma\) y \( \tilde\sigma\) son dos secciones meromorfas de
\( L\) el cociente \( \sigma/\tilde\sigma\) es una funci\'on meromorfa
sobre \( X\) por lo que \( d(\sigma)=d(\tilde\sigma)+d(\sigma/\tilde\sigma)\).

A los divisores de funciones meromorfas se les llama \emph{principales} y al subgrupo aditivo
de \( \Divisor(X)\) formado por dichos divisores lo denotaremos \( \PDivisor(x)\).
Con esta notaci\'on, podemos enunciar el siguiente teorema:




\begin{teorema}\label{teo:picard-divisores} 
	La aplicaci\'on que a cada haz de l\'ineas \( L\) sobre \( X\) le asigna
	la clase del divisor de alguna secci\'on meromorfa de \( L\) es un isomorfismo de grupos:
	\begin{align*}
		\pic(X)& \xrightarrow{\Divisor} \Divisor(X)/\PDivisor(X)\\
	L &\mapsto [d(\sigma)]
\end{align*}
\end{teorema}

Recordemos que el grado de cualquier funci\'on meromorfa es cero,
es decir, contando multiplicad tiene el mismo n\'umero de ceros que de polos.
Este hecho implica que si \( D\) es un divisor principal, entonces \( \grad(D)=0\),
por lo que el grado desciende al cociente \(\Divisor(X)/\PDivisor(X) \).

Una consecuencia de esta observaci\'on y el teorema anterior, es que
cualquier haz de l\'ineas \( L\) tiene un \emph{grado} bien definido
\( \grad(L)\) que de hecho es igual al grado de cualquier secci\'on meromorfa
de este.

Usando esta notaci\'on, podemos enunciar
otro resultado, tambi\'en muy importante y con numerosas aplicaciones
en la teor\'ia de superficies de Riemann, el teorema de Riemann-Roch:

\begin{teorema}[Riemann-Roch]\label{teo:riemann-roch}

Sea \(L\) un haz de l\'ineas sobre X. Entonces

\[\dimension_\mathbb{C}\H^0(L)-\dimension_\mathbb{C}\H^1(L) = \grad(L) - g +1\]
donde  \( g\) el g\'enero de \( X\).

\end{teorema}

Finalmente, un resultado debido a Jean Pierre Serre relaciona los \( 1\)-grupos
de cohomolog\'ia con los \( 0\)-grupos de cohomolog\'ia, y es la raz\'on por la cual
la teor\'ia se desarroll\'o originalmente sin menci\'on de la cohomolog\'ia%
\footnote{Para una discusi\'on de este hecho v\'ease \cite[p.~186]{donaldson2011riemann}.}


\begin{teorema}[Dualidad de Serre]\label{teo:dualidad-serre}
Sea \( L\) un haz de l\'ineas sobre \( X\). Entonces:
\[\H^0(L) \cong \H^1(L^*\otimes K_X)^*\]
o equivalentemente:
\[\H^0(L^*\otimes K_X) \cong \H^1(L)^*\]
\end{teorema}

Como primera aplicaci\'on de estos teoremas calcularemos la cohomolog\'ia
y el grado de los haces de l\'ineas m\'as importantes: el haz trivial y el haz can\'onico.


Primero notemos que el teorema de dualidad de Serre implica que:
\[1=\dim \H^0(\mathbb{C}_X) =\dim \H^1(K_X) \]
\[\dim \H^0(K_X) =\dim \H^1(\mathbb{C}_X) \]
Por otro lado, el teorema de Riemann-Roch implica que
\[\dimension\H^0(\mathbb{C}_X)-\dimension\H^1(\mathbb{C}_X) = \grad(\mathbb{C}_X) - g +1\]
que combinado con la afirmaci\'on anterior y que \( \grad(\mathbb{C}_X)=0\) se reduce a
\[ 1-\H^0(K_X)=-g+1\]
por lo que \( \H^0(K_X)=g\).
De nuevo usando el teorema de Riemann-Roch pero ahora para el haz can\'onico, tenemos:
\[\dimension\H^0(K_X)-\dimension\H^1(K_X) = \grad(K_X) - g +1\]
por lo que 
\[ g-1=\grad(K_X) -g +1\]
con lo que finalmente concluimos:
\begin{corolario} El grado del haz can\'onico de una superficie de Riemann compacta \( X\) es \( 2g- 2\) donde \( g\) es el g\'enero de la superficie:
	\[ \grad(K_X)=2g-2.\]
\end{corolario}

\vspace{1cm}
\centerline{\adforn{49} } 
\vspace{1cm}

Usando estos c\'alculos, f\'acilmente deduciremos la dimensi\'on del espacio
de diferenciales cuadr\'aticas \emph{holomorfas} sobre una superficie de Riemann compacta \( X\):

Como el grado es morfismo de grupos, tenemos que \( \grad(K_X^*)=-\grad(K_X)=2-2g\).
Analizaremos tres casos radicalmente distintos:

Si \( g>1\), \( \grad(K_X^*)<0\) por lo que \( K_X^*\) no tiene secciones holomorfas y consecuentemente \( \H^0(K_X^*)=0\).
Ahora bien, por el teorema de dualidad,
\[ \dim\H^0(Q_X)=\dim\H^0(K_X\otimes K_X)=\dim\H^1(K_X^*)\]
y para calcular este \'ultimo n\'umero podemos recurrir al teorema de Riemann-Roch:
\[ \dim\H^0(K_X^*)-\dim\H^1(K_X^*)=\grad(K_X^*)-g+1=3-3g\]
que junto con el hecho de que \( \H^0(K^*_X)=0\) implica que 
\( \dimension\H^0(Q_X)=3g-3\).


Si \( g=1\), entonces \( \dimension\H^0(K_X) =1\), por lo que existe al menos una diferencial  holomorfa.
Por otro lado, \( \grad(K_X)= 2g-2=0\), es decir, cualquier diferencial holomorfa tiene divisor nulo, por lo que no tiene ceros.
Esta \'ultima afirmaci\'on es equivalente a que \( K_X\) es un haz trivial, por lo que \( Q_X=K_X\otimes K_X\) es trivial tambi\'en.




En el caso \( g=0\), tenemos:  \( \grad(K_X)= 2g-2 = -2\) y \( \grad(Q_X) = -4\), por lo que \( Q_X\) no tiene secciones holomorfas.

Notemos adem\'as que, sabiendo que el grado del haz can\'onico es igual \( 2g-2\),
podemos deducir que el grado del haz \( Q_X\) es  \( 4g-4\).



\section{Cubierta espectral}%
\label{sec:cubierta-espectral}

El prop\'osito de esta secci\'on es construir la \emph{cubierta espectral}
asociada a una diferencial cuadr\'atica.

La cubierta espectral%
\footnote{El nombre lo tomamos directamente de \cite{bridgeland2015}. En otros contextos, como en la teor\'ia de superficies
planas, recibe el nombre de \emph{cubierta de orientaci\'on}, pues en esta cubierta la foliaci\'on asociada a la diferencial
cuadr\'atica es orientable.}
es una construcci\'on que permite tomar la <<ra\'iz cuadrada>> de la diferencial cuadr\'atica.
M\'as precisamente, la cubierta espectral es un cubriente ramificado de dos hojas de
la superficie original \( X\), con la propiedad de que el \emph{pullback} de la diferencial cuadr\'atica
a este cubriente se descompone como el cuadrado tensorial de alguna \( 1-\)forma meromorfa.
La importancia de esta construcci\'on radica entre otras cosas en que permite definir los periodos de la diferencial cuadr\'atica,
mismos que juegan un papel similar al de los periodos de diferenciales abelianas.%
\footnote{Para una exposici\'on breve sobre las \emph{coordenadas de periodos} para diferenciales abelianas, v\'ease \cite{wright2014translation}.}

Sea \( \varphi\) una diferencial cuadr\'atica meromorfa sobre \( X\).
Como primera aproximaci\'on, podr\'iamos intentar construir la cubierta espectral como el conjunto
\[ X_\varphi:=\{\alpha \in K_x| \alpha \otimes \alpha= \varphi(x)\}	\subseteq K_X\]
y tomar la la restricci\'on de la proyecci\'on de \( K_X\) sobre \( X\) como
la aplicaci\'on cubriente.

En este caso, resulta natural considerar la \( 1-\)forma meromorfa <<tautol\'ogica>>
que de manera concisa se podr\'ia expresar como%
\footnote{Esta expresi\'on no es del todo correcta, pero evitaremos dar m\'as detalles hasta
	que desarrollemos la versi\'on definitiva de la cubierta espectral.}
\begin{align*}
		X_\varphi & \xrightarrow{\omega} K_X \\
		\alpha &\mapsto \alpha
\end{align*}
y donde claramente \( \omega\otimes\omega=\varphi\).

El problema de esta construcci\'on es que localmente \( X_\varphi\) es
isomorfa a una variedad algebraica que puede tener singularidades,
o ni siquiera estar definida.
M\'as espec\'ificamente, si \( x\) es un cero de orden \( n\) de \( \varphi\)
es f\'acil ver que sobre una vecindad de \( x\) \( X_\varphi\) es isomorfa a la variedad
algebraica en \( \mathbb{C}^2\) asociada a la ecuaci\'on \( z^n=w^2\), que tiene
una singularidad en el origen si \( n>1\). Por otro lado, si \( \varphi\) tiene
un polo en \( x\), entonces \( X_\varphi\) no est\'a definida sobre \( x\).

Por esta raz\'on, lo primero que necesitaremos es una manera de modificar el haz \( Q_X\)
junto con la secci\'on \( \varphi\)
y para esto exploraremos con un poco m\'as de detalle el teorema \ref{teo:picard-divisores}.

Sea \( D\) un divisor sobre \( X\). Por el teorema \ref{teo:picard-divisores}
sabemos que existe un haz de l\'ineas que denotaremos \( L_D\) tal que
si \( \sigma\) es una secci\'on meromorfa de \( L_D\) entonces \( d(\sigma)\) est\'a
en la misma clase que \( D\). Sea \( \sigma\) cualquier secci\'on meromorfa de \( L_D\)
(que existe por el teorema \ref{teo:finitud}). Por la observaci\'on anterior,
existe una funci\'on meromorfa \( f\) tal que \( D=d(\sigma) +d(f)\).
Esto \'ultimo implica que la secci\'on \(\sigma_D:= f\sigma\) tiene divisor igual a \( D\).

Notemos que la propiedad \( d(\sigma_D)=D\) determina la secci\'on \( \sigma_D\) salvo multiplo escalar no nulo,
es decir, si \( \sigma^\prime\) tambi\'en tiene divisor igual a \( D\), entonces \( \sigma_D=C\sigma^\prime\)
donde \( C\) es una constante no nula.

Ahora  bien, la idea es tomar el producto tensorial del haz \( Q_X\) con un haz \( L_D\)
apropiado de tal forma que la secci\'on \( \varphi\otimes \sigma_D\) \'unicamente tenga 
ceros simples.

Sea \( D_\varphi= d(\varphi)\) el divisor de la diferencial cuadr\'atica \( \varphi\).
Si \( D_\varphi=\sum_{i=0}^n n_i p_i- \sum_{i=0}^m m_i q_i\), donde los \( n_i\)
y los \( m_i\) son enteros positivos, entonces definimos el divisor
\( D:=\sum_{i=0}^m 2 \left \lceil \frac{m_i}2 \right \rceil  q_i -\sum_{i=0}^n 2\left \lfloor \frac{n_i}2 \right \rfloor  p_i \).
Es claro que \( D_\varphi + D\) es cero en todo punto que no sea un cero o polo de orden impar de \( \varphi\),
y en ese caso vale \( 1\).

Consideremos ahora el haz \( Q_\varphi:= Q_X\otimes L_D\).
El producto tensorial \( \varphi\otimes \sigma_D\) es una secci\'on de \( Q_\varphi\)
con divisor igual a \( D_\varphi +D\), por lo que es una secci\'on holomorfa
con ceros simples exactamente en los ceros y polos de orden impar de \( \varphi\) y sin ceros de orden superior.

Notemos tambi\'en que si definimos el divisor
\( D/2:=\sum_{i=0}^m  \left \lceil \frac{m_i}2 \right \rceil  q_i -\sum_{i=0}^n \left \lfloor \frac{n_i}2 \right \rfloor  p_i\)
entonces \( 2(D/2)=D\) por lo que \( L_{D/2}\otimes L_{D/2} = L_D\).%
\footnote{Notemos que el divisor \( D\) es el \'unico que es <<divisible>> entre \( 2\)
y que cumple que \( \varphi\otimes\sigma_D\) es holomorfa y no tiene ceros de orden superior.}
Definamos \( K_\varphi:= K_X \otimes L_{D/2}\). Es claro que \( K_\varphi \otimes K_\varphi = Q_\varphi\).
Con esta notaci\'on, finalmente podemos definir la \emph{cubierta espectral}:

\begin{definicion}
La \emph{cubierta espectral} de la diferencial cuadr\'atica \( \varphi\)
es el conjunto
\[ X_\varphi := \{(x,\alpha)\in K_\varphi | \alpha \otimes\alpha = \varphi(x)\otimes \sigma_D(x) \}\subseteq K_\varphi.\]
Denotaremos la restricci\'on de la proyecci\'on de \( K_\varphi\) sobre \( X\) por \( \pi\).
\end{definicion}

\begin{proposicion}\label{prop:propiedades-cubierta-espectral}
	Sea \( X_\varphi\) la cubierta espectral asociada a \( \varphi\). Entonces:
	\begin{enumerate}
			 \item \( X_\varphi\) es una superficie de Riemann.
			\item La funci\'on \( \pi\) es una transformaci\'on holomorfa que exhibe a
	\( X_\varphi\) como cubriente ramificado de \( 2\) hojas de \( X\).
	Los puntos de ramificaci\'on de \( \pi\) son exactamente los ceros y polos de orden impar de \( \varphi\).
	\item Existe una \( 1-\)forma meromorfa \( \omega\) sobre \( X_\varphi\) tal que \( \omega\otimes\omega= \pi^*(\varphi)\).
	\end{enumerate}

\end{proposicion}
\begin{proof}
	Consideremos una trivializaci\'on del haz \( K_\varphi\) sobre un abierto \( U\).
	Esta trivializaci\'on induce una trivializaci\'on del haz \( Q_\varphi\), por lo que
	\( \varphi\otimes \sigma_D\) corresponde a una funci\'on holomorfa \( f\) sobre \( U\).
	Por esta raz\'on, \( \pi^{-1}(U)\) es equivalente al conjunto:
	\[ \{(x,z)\in U\times \mathbb{C}| z^2=f(x)\}\subseteq U\times \mathbb{C}.\]
	Como la funci\'on \( f\) es holomorfa y s\'olo tiene ceros simples,
	dicho conjunto no tiene singularidades. Como la trivializaci\'on es arbitraria,
	la afirmaci\'on anterior es v\'alida en una vecindad de cualquier punto de \( X_\varphi\),
	por lo que efectivamente es una superficie de Riemann.

	En cualquiera de dichas trivializaciones, la funci\'on \( \pi\) es equivalente a
	la proyecci\'on de \( U\times \mathbb{C}\) en la primera coordenada.
	De esto se deduce la afirmaci\'on \( 2\).

	Notemos que podemos escoger la secci\'on \( \sigma_{D/2}\)
	de tal forma que \( \sigma_{D/2}\otimes \sigma_{D/2}=\sigma_D\) y que
	si \( x\in X\) y \( \alpha\in (K_\varphi)_x\) entonces \( \frac\alpha{\sigma_{D/2}(x)}\in (K_X)_x\).
	Es decir \( \frac\alpha{\sigma_{D/2}(x)}\) es una forma lineal sobre \( \Tangente_xX\).

	Construiremos \( \omega\) describiendo su acci\'on en vectores tangentes:

	Si \( v\in \Tangente_{(x,\alpha)}X_\varphi\) definimos
	\[ \omega{(x,\alpha)}(v):= \frac\alpha{\sigma_{D/2}(x)}\left[\Tangente_{(x,\alpha)}\pi(v)\right]\]
	Como \( \Tangente_{(x,\alpha)}\pi\) es una transformaci\'on lineal y \( \frac\alpha{\sigma_{D/2}(x)}\)
	tambie\'n es lineal, \( \omega{(x,\alpha)}\) es una forma lineal en \( \Tangente_{(x,\alpha)}X_\varphi\)
	por lo que \( \omega\) es una \( 1-\)forma meromorfa en \( X_\varphi\). A \( \omega\) le
	llamaremos la forma tautol\'ogica.

	Ahora, si \( v\in \Tangente_{(x,\alpha)}X_\varphi\) entonces
	\( \omega\otimes \omega(v\otimes v)=\frac{\alpha\otimes \alpha}{\sigma_D(x)}[\Tangente_{(x,\alpha)}\pi(v) \otimes \Tangente_{(x,\alpha)}\pi(v)] \)
	pero como \( (x,\alpha)\in X_\varphi\) tenemos que \( \frac{\alpha\otimes \alpha}{\sigma_D(x)}=\frac{\varphi(x)\otimes \sigma_D(x)}{\sigma_D(x)}=\varphi(x) \) por lo que \( \omega\otimes \omega(v\otimes v)= \varphi(x)[\Tangente_{(x,\alpha)}\pi(v) \otimes \Tangente_{(x,\alpha)}\pi(v)]\).
	Es decir, \( \omega\otimes \omega= \pi^*(\varphi)\).

\end{proof}

La multiplicaci\'on por \( -1\), que es un isomorfismo del haz \( K_\varphi\),
se restringe a una involuci\'on holomorfa de \( X_\varphi\).
Denotaremos esta involuci\'on por \( \tau\); \( \tau\) preserva las fibras de \( \pi\)
y ademas \( \omega\) es \emph{antiinvariante} bajo \( \tau\), es decir \( \tau^*(\omega)=-\omega\).

Existe un \'unico campo vectorial meromorfo \( V_\omega\) que satisface \( \omega(V_\omega)=1\),
es decir, que es dual a la \( 1-\)forma \( \omega\).
Es claro que \( V_\omega\) es tangente a la foliaci\'on determinada por \( \omega\otimes\omega\),
por lo que provee de una orientaci\'on a esta. 
Al igual que \( \omega\), \( V_\omega\) es antiinvariante bajo \( \tau\).

A partir de estas observaciones se puede deducir que \( \pi\) manda las trayectorias
integrales de \( V_\omega\) en trayectorias horizontales de \( \varphi\);
la foliaci\'on asociada a \( \varphi\) es orientable si y solo si
\( X_\varphi\) tiene dos componentes conexas. Cada uno de los dos puntos
en una fibra gen\'erica de \( \pi\) corresponden a una de las posibles orientaciones
para la foliaci\'on y \( \tau\) intercambia dichas dos orientaciones.
Si adem\'as consideramos la m\'etrica asociada a la diferencial cuadr\'atica
\( \omega\otimes \omega\), \( \pi\) es una isometr\'ia salvo en los puntos de ramificaci\'on.

Como \( X_\varphi\) es un cubriente ramificado de dos hojas, en los puntos de ramificaci\'on
\( \pi\) es de grado dos, por lo que es localmente equivalente a la transformaci\'on
\( z\mapsto z^2\). Si \( x\in X\) es un punto cr\'itico de orden impar de \( \varphi\)
y expresamos la diferencial con respecto a una carta local \( \xi\), tenemos que
\( \varphi= \xi^{2n+1}d\xi^2\), por lo que al tomar el pullback bajo \( \pi\)
donde localmente \( \pi(z)=\xi(z)=z^2\), concluimos que:
\[ \pi^*(\varphi)=\xi(z)^{2n+1}d\xi(z)^2=z^{4n+2}2z^2dz^2=2z^{4n+4}dz^2.\]
Entre otras cosas, esto implica que la preimagen bajo \( \pi\) de un polo simple
de \( \varphi\) es un punto regular de \( \pi^*(\varphi)\).
En particular, si la diferencial \( \varphi\) \'unicamente tiene polos simples y ceros, la diferencial \( \omega\) es holomorfa,
es decir, es una diferencial abeliana.

\section{Homolog\'ia de la cubierta espectral}

En esta secci\'on definiremos la \emph{homolog\'ia gorro} que permite
ver a la diferencial como una clase de cohomolog\'ia, y esto a su vez
permite definir los periodos de la diferencial cuadr\'atica.

Por el resto de la secci\'on, fijaremos la diferencial cuadr\'atica \( \varphi\).
Denotaremos la cubierta espectral de \( X\) asociada a \( \varphi\) por
\(\widehat{X} \). Sea \( \omega\) la \( 1-\)forma meromofa tautol\'ogica.%
%\footnote{Recordemos que esta forma esta definida \'unicamente salvo signo.}

Denotaremos al conjunto de puntos cr\'iticos de \( \omega\) por 
\( \CritInf(\omega)\) y al complemento por \( \widehat X^\circ\).
Dado que \( \omega\) es holomorfa en \( \widehat X^\circ\), es una forma cerrada
y define una clase de cohomolog\'ia de de Rham.

Si \( [\alpha]\in \H_1(\widehat X)\) es una clase de homolog\'ia representada por
la \( 1-\)cadena \( \alpha\), podemos integrar \( \omega\) a lo largo de \( \alpha\)
y el resultado no depende del representante escogido.

La involuci\'on \( \tau\) de \( \widehat X\) induce una acci\'on en \( \H_1(\widehat X)\)
que tambi\'en es una involuci\'on, por lo que descompone dicho grupo como la suma de
una parte \( \tau-\)invariante y una parte \( \tau-\)antiinvariante:
\[ \H_1(\widehat X)= \H_1(\widehat X)^+ \!\oplus \H_1(\widehat X)^-\]
Si \( [\alpha]\) es una clase invariante, entonces se cumple que:
\[ \int_\alpha \omega = \int_{\tau(\alpha)}\omega= \int_\alpha \tau^*(\omega)= -\int_\alpha \omega\]
por lo que dicha integral se anula. Esto nos lleva a considerar \'unicamente
aquellas clases de homolog\'ia que son \( \tau-\)antiinvariantes.

\begin{definicion}
El grupo de \emph{homolog\'ia gorro} de la diferencial \( \varphi\) es la parte \( \tau-\)antiinvariante
del primer grupo de homolog\'ia de \( \widehat X^\circ\).
Denotaremos dicho grupo por \( \widehat{\H}(\varphi)\).
\end{definicion}

Como ya hemos mencionado, la diferencial cuadr\'atica \( \varphi\) induce un homomorfismo de grupos
que denotaremos \( \widehat{\varphi}\):
\begin{align*}
\widehat{\varphi}:\widehat{\H}(\varphi)&\xrightarrow{~~~~~~~} \mathbb{C}\\
	 [\alpha] &\mapsto  \frac12\int_\alpha \omega.
\end{align*}
%		\pic(X)& \xrightarrow{\Divisor} \Divisor(X)/\PDivisor(X)\\

A dicho homomorfismo, le llamaremos el \emph{periodo} de \( \varphi\).\label{pag:periodo}
As\'i mismo, a \( \widehat{\varphi}([\alpha])\) le llamaremos el \( \varphi-\)periodo de \( \alpha\), o m\'as concisamente, 
el periodo de \( \alpha\).


Es claro que \( \Hom_\mathbb{Z}(\widehat{\H}(\varphi),\mathbb{C})\cong \mathbb{C}^N\) donde \( N\) es el rango
del grupo de homolog\'ia gorro de \( \varphi\).
M\'as adelante encontraremos una forma de revertir esta construcci\'on y obtener una diferencial cuadr\'atica
a partir de un homomorfismo de grupos como el anterior. Por esto, ser\'a de gran relevancia conocer
el rango del grupo de homolog\'ia gorro, que es lo que calcularemos a continuaci\'on.

Como primer paso calculemos el g\'enero de \(\widehat X\) en t\'erminos del g\'enero de \( X\) y de los
ordenes de los ceros y polos de \( \varphi\).
Para esto usaremos la \emph{f\'ormula de Riemann-Hurwitz} aplicada a la proyecci\'on \( \pi\).
Como \( \pi\) es un cubriente ramificado de dos hojas, tiene grado \( 2\), tiene multiplicidad \( 2\) en los
puntos de ramificaci\'on y en el resto tiene multiplicidad \( 1\).

Con esta informaci\'on, la f\'ormula dice:
\[ 2\hat g -2 = 2(2g -2) + \#\{\textrm{puntos de ramificaci\'on}\}\]
donde \( \hat g\) es el g\'enero de \( \widehat X\) y \( g\) el g\'enero de \( X\).
Por la construcci\'on de la cubierta espectral, es claro que el n\'umero de puntos de ramificaci\'on
es la cantidad de ceros y polos de orden impar. En general, no se puede decir mucho m\'as
acerca de este n\'umero, sin embargo, para el caso de diferenciales GMN, como
\'unicamente tienen ceros simples, podemos calcular f\'acilmente la cantidad de estos:

Usando la notaci\'on de la secci\'on anterior, el divisor de la diferencial cuadr\'atica es
\( D_\varphi=\sum_{i=0}^{n} n_i p_i- \sum_{i=0}^m m_i q_i\) donde \( n_i=1\) para todo \( i\),
por lo que
\[4g-4= \grad(Q_X) = n-\sum_{i=0}^m m_i \]
es decir, 
\(n= 4g-4+\sum_{i=0}^m m_i \). Sea \( m_p\) el n\'umero de polos de orden par.
Con esta notaci\'on obtenemos:
\[ 2\hat g  = 4g -2 + (4g-4+\sum_{i=0}^m m_i)+ (m-m_p)\]
que es el rango del grupo \( \H_1(\widehat X)\).

Ahora bien, al remover \( k\) puntos de una superficie
el rango del primer grupo de homolog\'ia de la superficie aumenta en \( k-1\).
Este es un resultado cl\'asico en topolog\'ia algebraica que se puede demostrar,
por ejemplo, usando la sucesi\'on de Mayer-Vietoris.

Aplicando esto a la superficie \( \widehat X\) y dado que \( \CritInf(\omega)\) es
la preimagen de los puntos cr\'iticos infinitos de \( \varphi\), concluimos que
la cardinalidad de \( \CritInf(\omega)\) es \( m+m_p -s\) donde \( s\) es el 
n\'umero de polos simples de \( \varphi\). Esto \'ultimo se debe a que
los polos simples de \( \varphi\) se vuelven puntos regulares al pasar a la cubierta espectral,
y a que los polos de orden par tienen dos preim\'agenes en \( \widehat X\).
As\'i, obtenemos que:
\[ \rango \H_1(\widehat X ^\circ)= \rango \H_1(\widehat X) +(m+m_p -s-1)= 8g-6+\sum_{i=0}^m m_i+2m-s-1.\]

Sea \( X^\circ\) el complemento de los puntos cr\'iticos infinitos de \( \varphi\).
Entonces
\[ \rango\H_1(X^\circ)= \rango\H_1(X) +m-s-1=2g+m-s-1. \]

Es claro que \( X^\circ\) es el cociente bajo \( \tau\) de \( \widehat X^\circ\).
En general, para acciones de grupos finitos se tiene que%
\footnote{Esta afirmaci\'on es falsa para coeficientes enteros, de ah\'i la necesidad de pasar a coeficientes racionales.
	(En general basta pasar a un anillo de coeficientes en el que el orden del grupo sea un elemento invertible)
	V\'ease \cite{bredon1972introduction}, teorema \( 2.4\) p\'ag. \( 120\).}
\[ \H_\bullet(X,\mathbb{Q})^G \cong \H_\bullet(X/G,\mathbb{Q})\]
por lo que en este caso, podemos concluir que
\( \rango \H_1(X^\circ)= \rango \H_1(\widehat X^\circ)^+\).


Finalmente, juntando los c\'alculos anteriores, obtenemos que:
\begin{align*}
	\rango\widehat\H(\varphi)=\rango \H_1(\widehat X^\circ)^-=&\rango \H_1(\widehat X^\circ) -\rango \H_1(X^\circ)\\
	=& 8g-6+\sum_{i=0}^m m_i+2m-s-1 -(2g+m-s-1)\\
        = & 6g -6 + 	\sum_{i=0}^m m_i+m.
\end{align*}

Con esto hemos demostrado:
\begin{proposicion}\label{prop:dimension-h-gorro}
	El rango del grupo de homolog\'ia gorro de una diferencial GMN est\'a dado por
	\[ N(\varphi):=6g -6 + 	\sum_{i=0}^m m_i+m\]
	donde \( m\) es el n\'umero de polos de \( \varphi\) y \( m_i\) es el orden del \( i-\)\'esimo polo.

\end{proposicion}
Esta proposici\'on corresponde al lema 2.2 de \cite{bridgeland2015},
y en t\'erminos generales, la demostraci\'on sigue a la de dicho lema.


Antes de finalizar el cap\'itulo, observemos que la forma de intersecci\'on
de \( \H_1(\widehat X^\circ)\) se restringe una forma bilineal en \( \widehat \H(\varphi)\).
Dicha forma es degenerada pues de hecho es degenerada en \( \H_1(\widehat X^\circ)\).

Es por esto que definiremos otro apareamiento bilineal.
Recordemos que una manera de construir la forma de intersecci\'on, es combinando
el isomorfismo proveniente de la dualidad de Poincar\'e con el proveniente
del teorema de coeficientes universales.
Para el caso que nos interesa, en vez de usar la dualidad de Poincar\'e, usaremos
una versi\'on ligeramente m\'as general, la dualidad de Lefschetz:

\begin{teorema*}[Dualidad de Lefschetz]
Sea \( X\) una variedad orientada de dimensi\'on \( n\) con frontera.
Existe un isomorfismo
\[ D_L:\H^\bullet(X, \partial X) \rightarrow H_{n-\bullet}(X\setminus \partial X)\]
\end{teorema*}

En el caso de que \( X\) sea una superficie obtenemos un isomorfismo
entre \(\H^1(X, \partial X)\) y \({ H_1(X\setminus \partial X)}\).
Adem\'as, para el caso de las superficies que nos interesa, las superficies tienen homolog\'ia libre de torsi\'on,
por lo que el teorema de coeficientes universales
implica que 
\[ \H^\bullet(X,\mathbb{Z})\cong \Hom_\mathbb{Z}(\H_\bullet(X,\mathbb{Z}),\mathbb{Z}).\]
Combinando ambos isomorfismos podemos construir un apareamiento bilineal no degenerado:
\[ \langle ~~,~~\rangle: \H_1(X, \partial X)\times H_1(X\setminus \partial X)\rightarrow \mathbb{Z}\]

Ahora bien, \( \widehat X^\circ\) no es una superficie con frontera, sin embargo,
para cada punto de \( \CritInf(\omega)\) podemos considerar un peque\~no disco abierto al rededor de dicho punto.
Sea \( \widetilde X\) el complemento de dichos discos. Entonces es claro que \( \widehat X^\circ\) y
\( \widetilde X\) son homot\'opicas y que \( \widetilde X\) es una superficie con frontera.

Finalmente, usando la invarianza homot\'opica de la homolog\'ia, y el axioma de escisi\'on, se puede
construir un isomorfismo \( \H_1(\widetilde X, \partial \widetilde X)\cong\H_1(\widehat X, \CritInf(\omega))\) que
junto con el apareamiento bilineal definido anteriormente define un apareamiento:
\[ \langle ~~,~~\rangle: \H_1(\widehat X, \CritInf)\times H_1(\widehat X^\circ)\rightarrow \mathbb{Z}\]

Al igual que en el caso del apareamiento bilineal inducido por la dualidad de Poincar\'e,
este apareamiento se puede calcular como el n\'umero de intersecci\'on orientado de
curvas transversales que representen las clases de homolog\'ia. Usaremos este hecho
m\'as adelante al demostrar que las \emph{clases silla estandar} forman una base para
la homolog\'ia gorro de la superficie.

\endinput

\begin{teorema} Riemann-Hurwitz
	Si \( \phi: \mathcal{R}\rightarrow \mathcal{S}\) es una transformaci\'on holomorfa no constante
	entre dos superficies de Riemann compactas, entonces:
	\[\chi(\mathcal{R})=d(\phi)\chi(\mathcal{S}) - R_\phi \]
	donde \( \chi(\mathcal{S})\) es la car\'acter\'istica de Euler de \( \mathcal{S}\), \( d(\phi)\)
	es el \emph{grado} de \( \phi\) que se define como la cardinalidad de la preimagen de un punto
	gen\'erico en \( \mathcal{S}\) y \( R_\phi\) es el \'indice total de ramificaci\'on de \( \phi\).

\end{teorema}
