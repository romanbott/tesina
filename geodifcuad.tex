\chapter{Geometr\'ia de las diferenciales cuadr\'aticas}%
\label{cap:geometria-diferenciales}


En este cap\'itulo definiremos los objetos principales de estudio: las diferenciales cuadr\'aticas.
As\'i mismo, reproducimos algunos resultados cl\'asicos concernientes a
la geometr\'ia de las diferenciales cuadr\'aticas.
Todos los resultados que se presentan en este cap\'itulo son
resultados cl\'asicos que se pueden encontrar en multiples fuentes.
Nosotros escogimos seguir la presentaci\'on de \cite{strebel-quadratic}.

Para evitar que la presentaci\'on se complique incesariamente,
cuando un teorema no tenga referencia expl\'icita debe entenderse
que corresponde a un teorema en la fuente ya mencionada.
En caso contrario, daremos una referencia expl\'icita a la literatura.



\section{Diferenciales cuadr\'aticas}

Sea \( X\) una superficie de Riemann compacta y sea \( K_X\) el \emph{haz can\'onico} de \( X\),
es decir, el haz de l\'ineas sobre \( X\) cuya fibra \( K_x\) sobre cualquier punto
\( x \in X\) es el espacio cotangente\footnote{M\'as precisamente, el espacio cotangente \emph{complejo}.
	Cabe mencionar que el espacio cotangente \emph{real} a \( X\) es de dimensi\'on \( 2\)
	pues \( X\) tiene dimensi\'on real 2,
	mientras que el espacio \emph{complejo} tiene dimensi\'on compleja \( 1\) puesto que \( X\) tiene dimensi\'on compleja \( 1\).}
\(\Tangente_x^* X\)
a \( X\) en \(x\) .

Recordemos que las secciones holomorfas (resp. meromorfas) de \( K_X\)
no son otra cosa que las \( 1-\)formas holomorfas (resp. meromorfas) sobre
\( X\).\footnote{A las \(1\)-formas holomorfas tambi\'en se les conoce con el nombre de \emph{diferenciales abelianas}.}

Sea \(Q_X:=K_X^{\otimes 2}\) el haz cuadrado tensorial de \( K_X\).
\(Q_X\) es un haz de l\'ineas sobre \( X\) cuya fibra \(Q_x\)
para cualquier punto \( x\in X\) es can\'onicamente isomorfa a
\( \Tangente^*_x X\otimes_\mathbb{C}\Tangente^*_x X\).




\begin{definicion}
Una \emph{diferencial cuadr\'atica holomorfa} (resp. \emph{meromorfa}) sobre \(X\) es una secci\'on holomorfa (resp. meromorfa) de \( Q_X\).
\end{definicion}


Siempre que se habla de haces de l\'ineas (o m\'as en general, de haces fibrados),
existen dos maneras equivalentes de hacerlo:

La primera, es describir al haz en t\'erminos globales,
es decir, mediante una variedad compleja \(2\)-dimensional \( L\)
y una proyecci\'on \( p:L \rightarrow X\) que es una sumersi\'on
y que cumple que \( L_x:= p^{-1}(x)\) tiene la estructura de un espacio vectorial complejo de dimensi\'on \( 1\).


La segunda, es describir al haz en t\'erminos locales,
es decir, mediante una cubierta abierta \(  \mathcal{U}\) de \( X\)
y \emph{funciones de transici\'on} \(\{ \phi_{U,V}\}_{(U,V)\in  \mathcal{U}\times  \mathcal{U}}\)
de tal forma que \[ \phi_{U,V}:U \cap V\rightarrow\GeneralLinear(1,\mathbb{C})\cong \mathbb{C}^*\]
y donde dichas funciones de transici\'on cumplen una cierta condici\'on de \emph{cociclo}.

Esta segunda forma de describir los haces de l\'ineas,
permite identificar el conjunto de clases de isomorfismo de haces de l\'ineas sobre \( X\)
con el grupo \( \H^1 (X,\mathcal{O}^*_X)\), el primer grupo de cohomolog\'ia
de gavillas de la gavilla \( \mathcal{O}^*_X\) de funciones holomorfas que no se anulan.


Denotemos como \(\pic(X)\) al conjunto de clases de isomorfismo de haces de l\'ineas sobre \(X\).
El producto tensorial de haces induce una estructura de grupo en \( \pic(X)\)
cuyo neutro es el haz trivial \( \mathbb{C}_X:= X\times \mathbb{C}\) y donde el inverso de un haz
\( L\) es el haz dual \( L^*\).
A dicho grupo se le llama el \emph{grupo de Picard} de \( X\).
La identificaci\'on antes mencionada induce un isomorfismo de grupos:
\[ \pic(X)\cong \H^1(X,\mathcal{O}^*_X).\]

De la misma forma en que los haces de l\'ineas se pueden describir en t\'erminos locales o globales,
las secciones de dichos haces tambi\'en se pueden describir de ambas formas.
De manera global, una secci\'on \( \sigma\) de un haz \( p:L \rightarrow X\)
es una funci\'on holomorfa \( \sigma: X\rightarrow L\) tal que \( p\circ \sigma =\Id_X\).
Por otro lado, de manera local, una secci\'on \( \sigma \) de un haz descrito mediante funciones de transici\'on
\(\{ \phi_{U,V}\}_{(U,V)\in  \mathcal{U}\times  \mathcal{U}}\) definidas en una cubierta \( \mathcal{U}\)
es una colecci\'on de funciones holomorfas \(\{\sigma_U:U\rightarrow \mathbb{C}\}_{U\in  \mathcal{U}}\)
que cumplen:
\[ \sigma_U=\sigma_V\phi_{U,V}\]
en el dominio com\'un \( U\cap V\).

Si \( \sigma\) es una secci\'on de un haz de l\'ineas \( L\) y \( \tau\) es una secci\'on de \( L^\prime\)
entonces de manera natural \( \sigma\otimes \tau\) es una secci\'on de \( L\otimes L^\prime\).
Adem\'as, se cumple que si \(  U\) es una trivializaci\'on de \( L\otimes L^\prime\)
entonces \( (\sigma\otimes \tau)_ U= \sigma_ U\tau_ U\), es decir,
en una trivializaci\'on el producto tensorial de secciones corresponde al producto usual de funciones holomorfas.

Sea \( z: U \rightarrow \mathbb{C}\) una carta de \( X\). Para cada punto \( x\in  U\)
la diferencial de \( z\), es un isomorfismo \( \mathbb{C}-\)lineal \( dz_p:\Tangente_p X\rightarrow \Tangente_{z(p)}\mathbb{C}\cong \mathbb{C}\), es decir, \( dz_p \in \Tangente_p^* X\).
De aqu\'i, es f\'acil concluir que \( dz\) es de manera natural una secci\'on sobre \(  U\) de \(K\), y m\'as a\'un,
una secci\'on que no se anula en ning\'un punto.
Como \( K_p\) es de dimensi\'on \( 1\), cualquier secci\'on \( \sigma\) es (puntualmente) un m\'ultiplo escalar de \( dz\),
es decir, en \(  U\) \( \sigma=f(z)dz\) donde \( f\) es una funci\'on holomorfa.

De igual forma, \( dz\otimes dz\) es una secci\'on de \( Q\) que no se anula en ning\'un punto,
por lo que cualquier secci\'on \( \varphi\) de \( Q\) se puede escribir como
\( f(z)dz\otimes dz\) en \(  U\). Usualmente \( dz\otimes dz\) se abrevia como \(  dz^2\).

Recordemos que si \( \phi:  U \rightarrow V\) es un biholomorfismo
entre dos abiertos del plano complejo, y \(\omega=  f(z)dz\) es una \( 1-\)forma holomorfa
sobre el abierto \( V\), entonces el \emph{pullback} de \( \omega\) bajo \( \phi\)
es la \( 1-\)forma holomorfa \( \phi^*(\omega)= f(\phi(z))\frac{d\phi}{dz}dz\).

As\'i mismo, si \( \varphi= f(z)dz^2\) es una diferencial cuadr\'atica, definimos
el \emph{pullback} de \( \varphi\) bajo \( \phi\) como \( \phi^*(\varphi)= f(\phi(z))\left (\frac{d\phi}{dz}\right)^2dz^2\).
Esta definici\'on es necesaria si se quiere que el pullback sea compatible con el producto tensorial, es decir,
si \( \omega\) y \( \omega^\prime\) son dos \( 1-\)formas holomorfas, se tiene que
\( \phi^*(\omega\otimes \omega^\prime)=\phi^*(\omega)\otimes\phi^*(\omega^\prime)\).
A partir de esto, es claro que si \( z: U \rightarrow \mathbb{C}\) es una carta en \( X\),
\( dz^2\) es la diferencial cuadr\'atica definida con anterioridad, y \( \varphi\) es una diferencial cuadr\'atica
tal que \( \varphi=f(z)dz^2\), entonces 
\[ (z^{-1})^*(\varphi)=f(z)dz^2\]
donde el lado de la derecha es una diferencial cuadr\'atica en \( \mathbb{C}\).

La afirmaci\'on anterior, implica que, si  \( z:  U \rightarrow \mathbb{C}\) y
\( w: V\rightarrow \mathbb{C}\) son dos cartas en \( X\) y \( \varphi\) es una diferencial
cuadr\'atica sobre \( X\) tal que \( \varphi = f(z)dz^2 = g(w)dw^2\), entonces
\[ f(z)dz^2= f(z(w))\left(\frac{z(w)}{dw}\right)^2dw^2\]
es decir, el an\'alogo de la regla de la cadena es v\'alido tambi\'en para diferenciales cuadr\'aticas.





\section{M\'etrica y foliaci\'on asociadas a una diferencial}


Como ya hemos mencionado con anterioridad, las diferenciales cuadr\'aticas
determinan una m\'etrica y una foliaci\'on (ambas con singularidades) sobre
la superficie de Riemann.



Sea \( X\) una superficie de Riemann y \(\varphi\) una diferencial cuadr\'atica meromorfa%
\footnote{Este trabajo se enfoca, principalmente, en diferenciales con polos dobles.}%
sobre \( X\).
\begin{definicion}
Un punto de \( X\) en el que \(\varphi\) no se anula y tampoco tiene un polo recibe el nombre de \emph{punto regular de \( \phi\)}. En caso contrario, recibe el nombre de \emph{punto cr\'itico}. Un punto cr\'itico es \emph{finito} si es un cero, o un polo de orden \(1\); si es un polo de orden al menos \( 2\) recibe el nombre de \emph{punto cr\'itico infinito}.
\end{definicion}

Supongamos que \( p\) es un punto regular de \(\phi\). Entonces es claro que podemos
encontrar una  vecindad contraible \( U\) de \( p\) sin puntos cr\'iticos de \( \varphi\)
y en la que \( \varphi= f(z)dz^2\).
En dicha vecindad definimos la funci\'on:
\[ \psi(q):= \int_p^q \sqrt{f(z)}dz\]
Donde \( \sqrt{f(z)}\) es una rama de la ra\'iz cuadrada de \( f\).

Dicha funci\'on es holomorfa y est\'a bien definida%
\footnote{Es decir, no depende del camino que conecta a \( p\) y a \( q\) escogido para realizar la integraci\'on.}
pues \(\sqrt{f(z)}dz \) es una \( 1\)-forma holomorfa.
Adem\'as, tiene derivada no nula, por lo que cerca de \( p\) es un biholomorfismo.

Un c\'alculo sencillo muestra que \( (\psi^{-1})^*(\varphi)= dz^2\) la diferencial cuadr\'atica
natural de \( \mathbb{C}\). Con esto hemos demostrado:

\begin{proposicion}\label{cartas-normales}
Alrededor de cualquier punto regular \( p\) de \( \varphi\) existe una carta
{\(\psi :U\rightarrow \mathbb{C}\)}
tal que \( (\psi^{-1})^*(\phi)= dz^2\).
Adem\'as, toda carta que satisfaga esta propiedad se puede escribir de la forma
\[ \psi(q)=\int_p^q\sqrt{f(z)}dz+b\]
donde \( b\) es una constante y \( \varphi=f(z)dz^2\).
\end{proposicion}

Las cartas que satisfacen la conclusi\'on de la proposici\'on anterior
jugar\'an un papel fundamental para definir la m\'etrica y la foliaci\'on
determinada por \( \varphi\).

\begin{definicion}
	Llamaremos \emph{carta normal} (para \( \varphi\)) a cualquier carta coordenada \( z\)
	de \( X\) tal que \( \varphi=dz^2\).
\end{definicion}

Notemos que si \( \psi:U\rightarrow V\) es un biholomorfismo
entre abiertos del plano complejo tal que \( \psi^*(dz^2)=dz^2\), entonces
necesariamente \(( \frac{d\psi}{dz})^2dz^2=dz^2\) es decir, \(( \frac{d\psi}{dz})^2=1 \)
por lo que \( \psi(z)=\pm z +b\).

Este hecho implica que los cambios de coordenadas entre dos coordeandas normales
son precisamente de esa forma:
\[ z\mapsto \pm z+b.\]
En cualquier caso, la m\'etrica euclidiana y la foliaci\'on horizontal%
\footnote{Es decir, la foliaci\'on de \( \mathbb{C}\) cuyas hojas son las rectas horizontales.
	De hecho, \emph{todas} las foliaciones por rectas paralelas se preservan bajo dichos cambios de coordenadas,
sin embargo, la foliaci\'on que es m\'as relevante en este trabajo es la foliaci\'on horizontal.}
de \( \mathbb{C}\)
son invariantes bajo dichos cambios de coordenadas.


\begin{definicion}\label{def:metrica-foliacion}
La m\'etrica determinada por \( \varphi\) es la \'unica m\'etrica que coincide
con la m\'etrica euclidiana en cada carta normal.

De igual forma definimos la foliaci\'on determinada por \( \varphi\).
A dicha foliaci\'on usualmente se le llama \emph{foliaci\'on horizontal} y
a las hojas de dicha foliaci\'on \emph{trayectorias horizontales} de la diferencial.
\end{definicion}

Ambas estructuras est\'an bien definidas pues son invariantes bajo
los cambios de coordenadas normales.

Usando la proposici\'on \ref{cartas-normales} no es dif\'icil demostrar que la m\'etrica
est\'a dada localmente por \( |\sqrt{f(z)}||dz|\) y que
la foliaci\'on coincide con los conjuntos de nivel de la funci\'on
\[ q\mapsto \im\left(\int_p^q\sqrt{f(z)}dz\right)\]
donde \( \im(\cdot)\) denota la parte imaginaria.

Como la m\'etrica es localmente isom\'etrica a la m\'etrica euclidiana,
es claro que la m\'etrica es plana. Adem\'as es claro que las hojas
de la foliaci\'on horizontal (apropiadamente parametrizadas) son geod\'esicas de la m\'etrica.

\begin{observacion}
	Hasta ahora \'unicamente hemos definido la m\'etrica y la foliaci\'on
	para el complemento de los puntos cr\'iticos. M\'as adelante veremos que
	la m\'etrica se puede definir tambi\'en en los puntos cr\'iticos finitos
	(aunque no ser\'a una m\'etrica riemanniana en el sentido usual)
	y que en cierto modo, los puntos cr\'iticos infinitos,
	desde el punto de vista m\'etrico, son puntos al infinito.
	 \end{observacion}


%\begin{proposicion}
%foliacion y metrica determinan la estructura compleja y la diferencial
%\end{proposicion}



\subsection{Geometr\'ia local de la foliaci\'on}

Por la forma en que se defini\'o la m\'etrica y la foliaci\'on determinadas por una diferencial cuadr\'atica,
es claro que ambas estructuras son localmente triviales alrededor de cualquier punto regular.
De esto se deriva el hecho de que para entender el comportamiento local
de dichas estructuras, basta entender qu\'e ocurre cerca de los puntos cr\'iticos.
En esta subsecci\'on veremos que existe una clasificaci\'on suficientemente expl\'icita de dicho comportamiento.

Enunciaremos el resultado principal a tal respecto y posteriormente
daremos un breve esbozo de la demostraci\'on.

\begin{teorema}\label{teo:criticos}
	Sea \( p\) un punto cr\'itico de \( \varphi\). Supongamos que el orden de \( \varphi\) en \( p\) es \( n\)
	con \( n>0\) o \( n=2k+1<0\), es decir, \( p\) es un cero o un polo de orden impar.
	Entonces existe una carta \( z\) centrada en \( p\) tal que 
	\[ \varphi=Cz^ndz^2,\]
	donde \( C\) es una constante positiva.

	Si \( \varphi\) es de orden \( n=2k<0\) en \( p\), es decir, es \( p\) es un polo de orden par,
	entonces existe una carta \( z\) centrada en \( p\) tal que
	\[ \varphi= \left( Cz^k +bz^{-1} \right)^2dz^2,\]
	donde \( C\) es una constante positiva y \( b\) es un n\'umero complejo.
\end{teorema}

El teorema anterior permite clasificar por completo el comportamiento de la foliaci\'on y m\'etrica
cerca de los puntos cr\'iticos:

\begin{teorema}\label{teo:forma-criticos-finitos}
	Sea \( \varphi=z^ndz^2\) con \( n\geq -1\).
	Entonces los \( n+2\) semirayos que parten del origen con \'angulos
	\( k\frac{2\pi}{n+2}\) con \( k=0,1,\ldots,n+1\) son hojas de la foliaci\'on asociada a \( \varphi\).

	Dichos semirayos dividen al plano en \( n+2\) sectores, cada uno de los cuales tiene una m\'etrica
	y foliaci\'on isomorfas a la m\'etrica euclidiana y la foliaci\'on horizontal del semiplano superior.
	El isomorfismo est\'a dado por el biholomorfismo \( z\mapsto \left(\frac2{n+2}\right)^2z^{\frac{n+2}2}\)
	que est\'a bien definido en
	cada uno de dichos sectores.
\end{teorema}
Usando el teorema \ref{teo:forma-criticos-finitos} es posible encontrar expl\'icitamente la foliaci\'on
cerca del origen para las diferenciales cuadr\'aticas \( \varphi=z^ndz^2\) con \( n\geq -1\).
En la figura \ref{fig:foliacion-criticos-finitos} mostramos algunas trayectorias horizontales
para dichas diferenciales cuadr\'aticas con \( n<3\).
\tikzexternalenable
\begin{figure}
\centering
\tikzsetnextfilename{n1}
\subbottom[$\frac1zdz^2$]{\input{fig/n1.pgf}}
\tikzsetnextfilename{p0}
\subbottom[$dz^2$]{\input{fig/p0.pgf}}
\tikzsetnextfilename{p1}
\subbottom[$zdz^2$]{\input{fig/p1.pgf}}
\tikzsetnextfilename{p2}
\subbottom[$z^2dz^2$]{\input{fig/p2.pgf}}
\caption{Trayectorias horizontales de $z^ndz^2$. En estas im\'agenes, las trayectorias horizontales son equidistantes en la m\'etrica de la diferencial, lo que permite apreciar el comportamiento de la m\'etrica al aproximarse a un punto cr\'itico finito.}\label{fig:foliacion-criticos-finitos}
\end{figure}
\tikzexternaldisable
De manera totalmente an\'aloga, tenemos el siguiente teorema:

\begin{teorema}\label{teo:forma-criticos-infinitos}
	Sea \( \varphi=z^{-n}dz^2\) con \( n\geq 3\).
	Entonces los \( n-2\) semirayos que parten del origen con \'angulos
	\( k\frac{2\pi}{n-2}\) con \( k=0,1,\ldots,n-3\) son hojas de la foliaci\'on asociada a \( \varphi\).

	Dichos semirayos dividen al plano en \( n-2\) sectores, cada uno de los cuales tiene una m\'etrica
	y foliaci\'on isomorfas a la m\'etrica euclidiana y la foliaci\'on horizontal del semiplano superior.
	El isomorfismo est\'a dado por el biholomorfismo \( z\mapsto \left(\frac2{2-n}\right)^2z^{\frac{2-n}2}\)
	que est\'a bien definido en
	cada uno de dichos sectores.
\end{teorema}

Notemos que en este caso, a diferencia del teorema \ref{teo:forma-criticos-finitos},
todas las trayectorias horizontales, con excepci\'on de los semirayos, convergen
en ambas direcciones al polo en cuesti\'on.

En la figura \ref{fig:foliacion-criticos-infinitos} mostramos algunas trayectorias horizontales
para las diferenciales \( z^{-n}dz^2\) con \( n<7\).

\tikzexternalenable
\begin{figure}[h]
\centering
\tikzsetnextfilename{n32}
\subbottom[$z^{-3}dz^2$]{\input{fig/n32.pgf}}
\tikzsetnextfilename{n42}
\subbottom[$z^{-4}dz^2$]{\input{fig/n42.pgf}}
\tikzsetnextfilename{n52}
\subbottom[$z^{-5}dz^2$]{\input{fig/n52.pgf}}
\tikzsetnextfilename{n6}
\subbottom[$z^{-6}dz^2$]{\input{fig/n6.pgf}}
\caption{Trayectorias horizontales de $z^{-n}dz^2$. Al igual que en la figura anterior, las trayectorias son equidistantes en la m\'etrica inducida por la diferencial.}\label{fig:foliacion-criticos-infinitos}
\end{figure}
\tikzexternaldisable

En el caso de las diferenciales \( \left(z^{-n} +\frac b{z}\right)^2dz^2\) con \( b\neq 0\) y \( n>1\)
ya no es posible encontrar un biholomorfismo expl\'icito entre un sector delimitado por dos semirayos
y el semiplano complejo. Sin embargo, se puede demostrar que existe una vecindad del origen
en la que la foliaci\'on es isomorfa a la foliaci\'on horizontal de \( z^{-2n}dz^2\).

Finalmente, el siguiente teorema describe el comportamiento de la foliaci\'on y m\'etrica para el caso
\( bz^{-2}dz^2\).

\begin{teorema}\label{teo:forma-polos-dobles}
	Sea \( \varphi=b^2z^{-2}dz^2\) con \( b\neq 0\) y consideremos la m\'etrica y foliaci\'on horizontal asociadas a \( \varphi\).
	Sea \( \psi:\mathbb{C} \rightarrow \mathbb{C}\setminus \{0\}\) la aplicaci\'on cubriente:
	\[ z\xmapsto{\psi} e^{z/b}\]
	Entonces \( \psi\) es una isometr\'ia local entre el plano y
	\( \mathbb{C}\setminus \{0\}\) con la m\'etrica asociada a \( \varphi\)
	que lleva rectas horizontales en trayectorias horizontales.
\end{teorema}

Del teorema anterior, se concluye que si \( b\) es imaginario puro, la foliaci\'on es por c\'irculos conc\'entricos.
Si \( b\) es real, la foliaci\'on es por rayos que convergen al origen, y en los dem\'as casos es por espirales logar\'itmicas.
Lo relevante en este caso es que si \( b\) no es imaginario, entonces cada trayectoria converge al origen en una direcci\'on.

Para finalizar esta subsecci\'on, resumiremos los teoremas anteriores
con las siguientes observaciones:
\begin{itemize}
		 \item Alrededor de cualquier punto cr\'itico infinito existe una vecindad
			 con la propiedad de que cualquier trayectoria horizontal que la intersecte
			 converge al punto cr\'itico en al menos una direcci\'on.

		\item Desde el punto de vista m\'etrico, los puntos cr\'iticos infinitos
			son puntos al infinito.
		\item Alrededor de cualquier punto cr\'itico finito de orden \( n\) existe una vecindad
			con la propiedad de que exactamente \( n+2\) trayectorias horizontales
			convergen al punto cr\'itico.
		\item Los puntos cr\'iticos finitos est\'an a una distancia finita de cualquier punto regular.
\end{itemize}

%definici\'on alternativa: superficie con cambios de coordenadas blabla y singularidades c\'onicas.

%equivalencia entre superficies planas y diferenciales cuadr\'aticas






\begin{proof}[Esbozo de demostraci\'on del teorema \ref{teo:criticos}]
	Consideremos el caso en que \( p\) es un cero de orden impar de la diferencial cuadr\'atica.
	Sea \( \xi\) una coordenada centrada en \( p\). Entonces tenemos que
	\[ \varphi=\xi^{2k+1}f(\xi)d\xi^2\]
	donde \( k\) es un entero no negativo, y \( f\) es holomorfa y no se anula en cero.
	
	Como primer paso, consideremos la diferencial abeliana
	\[\omega = \sqrt{\varphi}=\xi^{\frac{2k+1}{2}}\sqrt{f(\xi)}d\xi \]
	que est\'a bien definida y es univaluada en abiertos arbitrariamente cercanos al origen,
	pero suficientemente peque\~nos.

	Si \(z\) es una coordenada centrada en \( p\), entonces
	\[ \varphi=z^*(Cz^{2k+1}dz^2)\]
	si y s\'olo si
	\[ \omega=z^*(\pm Kz^{\frac{2k+1}{2}}dz),\]
	donde \( K^2=C\), 
	por lo que nos concentraremos en obtener una carta \( z\) que cumpla esta propiedad.

	No es muy dif\'icil convencerse de que encontrar dicha carta es equivalente a resolver
	la ecuaci\'on diferencial ordinaria a tiempo complejo:
	\[ \xi^{\frac{2k+1}{2}}\sqrt{f(\xi)}=\pm Kz(\xi)^{\frac{2k+1}{2}}\frac{dz(\xi)}{d\xi}.\]
	Para resolver dicha ecuaci\'on, podemos expandir \( \sqrt{f(\xi)}\) en serie de potencias
	y despu\'es integrar t\'ermino a t\'ermino para obtener:
	\[ \xi^{\frac{2k+3}{2}}g(\xi)=\int\frac{dz(\xi)^\frac{2k+3}{2}}{d\xi}d\xi=z(\xi)^\frac{2k+3}{2},\]
	que finalmente se traduce en:
	\[ z(\xi):=\xi g(\xi)^{\frac{2}{2k+3}}\]
	donde \( g(\xi)^{\frac{2}{2k+3}}\) est\'a bien definida y es univaluada en una vecindad del origen
	pues \( g(0)\neq 0\), y \( z\) es un biholomorfismo cerca del origen pues \( \frac{dz(\xi)}{d\xi}(0)=g(0)^{\frac{2}{2k+3}}\neq 0\).

	Con esto se demuestra el teorema \ref{teo:criticos} para el caso de ceros de orden impar.
	El caso de ceros de orden par y de polos de orden impar es muy similar.
	El \'unico caso que es ligeramente m\'as sutil es cuando el polo es de orden par,
	pues al resolver la ecuaci\'on diferencial asociada e integrar la expresi\'on
	\[\xi^{-n}\sqrt{f(\xi)} \]
	pueden aparecer t\'erminos logar\'itmicos, y estos son la raz\'on por la cual
	en este caso \'unicamente se puede encontrar una carta en la que
	\[ \varphi= \left( Cz^k +bz^{-1} \right)^2dz^2.\]
\end{proof}



\section{Separatrices y descomposici\'on de la superficie}

Usando los resultados de la secci\'on anterior, en esta secci\'on daremos una primera
clasifiaci\'on global de las trayectorias horizontales de una diferencial cuadr\'atica.

\begin{definicion}
	Sea \( \gamma\) una trayectoria horizontal de la diferencial cuadr\'atica \( \varphi\). Decimos que \( \gamma\) es:
	\begin{itemize}
			 \item \emph{gen\'erica}, si converge a puntos cr\'iticos infinitos en ambas direcciones;
			\item una trayectoria \emph{silla} si converge a puntos cr\'iticos finitos en ambas direcciones;
			\item una \emph{separatriz} si en una direcci\'on converge a un punto cr\'itico infinito
				y en la otra direcci\'on a un punto cr\'itico finito;
			\item \emph{peri\'odica} si es difeomorfa a un c\'irculo;
			\item \emph{divergente} en el resto de los casos.%
				\tablefootnote{El t\'ermino \emph{divergente} puede ser confuso: dado que la superficie es compacta,
				toda trayectoria no compacta tiene puntos l\'imite. De hecho, una trayectoria es divergente si en al menos una direcci\'on, tiene m\'as de un punto l\'imite. V\'ease \cite[pag.~45]{strebel-quadratic}.}

	\end{itemize}
\end{definicion}

Como primera observaci\'on, notemos que ninguna trayectoria
satisface simult\'aneamente dos de las condiciones de la
definici\'on anterior.

En lo sucesivo, cuando hablemos de una parametrizaci\'on \( \gamma\) de una trayectoria horizontal,
asumiremos impl\'icitamente que dicha parametrizaci\'on es unitaria, es decir,
\( |\dot{\gamma}|=1\), o equivalentemente, que en una carta normal satisface
\( \gamma(t)-\gamma(s)=\pm|t-s|\).

Una de las herramientas b\'asicas que usaremos en repetidas ocasiones ser\'a
la siguiente proposici\'on, que permite encajar cualquier segmento de trayectoria horizontal
en un rect\'angulo foliado por trayectorias horizontales de la misma longitud que \( \gamma\):

\begin{proposicion}
Si \( \gamma\) es una trayectoria definida en un intervalo \( [a,b]\)
entonces existe \( \epsilon >0 \) y una carta normal \( z\) tal que 
\( z(\gamma)=[a,b]\subseteq \mathbb{C}\) y \( z\) cubre al rect\'angulo
\( R=\{z\in \mathbb{C}|a<\re(z)<b, |\im(z)|<\epsilon\}.\)
\end{proposicion}
\begin{proof}[Esbozo de demostraci\'on]
	Alrededor de cualquier punto de \( \gamma\) existe una carta normal
	que cubre un rect\'angulo de una cierta altura y donde la imagen de \( \gamma\)
	est\'a contenida en el eje real.
	Por la compacidad del intervalo \( [a,b]\) se puede escoger
	una cantidad finita de dichas cartas que cubran \( \gamma\),
	por lo que existe un \( \epsilon>0\)
	tal que cualquiera de estas cartas cubre un rect\'angulo de altura mayor que \( \epsilon\).

	Por la forma tan particular que tienen los cambios de coordenadas normales,
	es posible reparametrizar las cartas antes mencionadas para que sean compatibles
	y definan una \'unica carta con las propiedades buscadas.

	Otra forma de demostrar esta proposici\'on es usar la aplicaci\'on exponencial
	en la direcci\'on ortogonal a \( \gamma\) que, dado que la m\'etrica es plana,
	es una isometr\'ia.
\end{proof}
\begin{corolario}\label{cor:rectangulo-cubriente}
	Si adem\'as existe un segmento vertical de longitud \( \delta\), que
	inicia en \( \gamma(a)\) y con la propiedad de que cualquier trayectoria
	horizontal que parte de alg\'un punto en el segmento en la misma direcci\'on que \( \gamma\)
	se puede extender hasta una longitud \( b-a\), entonces existe una isometr\'ia local
	\[ \psi: \{z\in\mathbb{C}|a<\re(z)<b, 0\leq \im(z)<\delta\}\rightarrow X\]
	tal que \( \psi([a,b])=\gamma\),
	\( \psi (i[0,\delta])\) es el segmento vertical y \( \psi^{-1}\) es localmente una carta normal.
\end{corolario}

Sea \( \gamma\) una trayectoria divergente.
Consideremos una parametrizaci\'on definida en un intervalo maximal:
\[ \gamma: (a,b)\rightarrow X.\]
Dado que \( \gamma\) es divergente, al menos en una direcci\'on no converge a
ning\'un punto cr\'itico. Adem\'as, como \( X\) es compacta,
\(\gamma\) se puede extender de manera indefinida en dicha direcci\'on.
Sin p\'erdida de generalidad, podemos suponer entonces que \( \gamma\) est\'a
definida en el intervalo \( (a,\infty)\).

Si \( a<t_0<t_1<\ldots\) es cualquier sucesi\'on creciente de n\'umeros reales
con \({ t_n \to \infty}\), entonces, nuevamente gracias a la compacidad de \( X\),
\( \gamma(t_n)\) tiene al menos un punto de acumulaci\'on \( p\).
Como \( \gamma\) es divergente, \( p\) no puede ser un punto cr\'itico infinito.
Por la misma raz\'on, en el caso de que \( p\) sea un punto cr\'itico finito,
\( \gamma\) no puede aproximarse al punto en una de las direcciones distinguidas.
En este caso, cada una de las trayectorias que pasan por \( \gamma(t_n)\) tambi\'en
forman parte de \( \gamma\) por lo que es claro que por lo menos un rayo
que parte del cero es subconjunto de los puntos l\'imite de \( \gamma\).

De cualquier forma, una trayectoria divergente tiene m\'as de un punto l\'imite
y usando esta propiedad podemos demostrar la siguiente proposici\'on:


\begin{proposicion}\label{prop:divergente-recurrente}
	Sea \( \gamma\) una trayectoria divergente. Supongamos que \( \gamma\) est\'a parametrizada de tal forma
       que el conjunto de puntos l\'imite de \( \gamma(t)\) cuando \( t \to \infty\) tiene m\'as de un punto.
	Sea \( B\) un intervalo vertical que empieza en \( P_0=\gamma(t_0)\), entonces para cualquier punto \( P_1=\gamma(t_1)\) despu\'es de \( P_0\) (\ie \( t_1>t_0\))
	existe  otro punto \( P_2\) despu\'es de \( P_1\) tal que \( \gamma\) intersecta a \( B\) en \( P_2\) 
	en la misma direcci\'on que en \( P_0\).
\end{proposicion}
\begin{proof}
Haciendo al intervalo \( B\) suficientemente peque\~no, podemos suponer que ninguna trayectoria que pase
por un punto en \( B\) es una trayectoria peri\'odica, ni tiende a un punto cr\'itico finito.

Esto implica que cualquier trayectoria que pase por \( B\) se puede extender indefinidamente en la misma direcci\'on que \( \gamma\).

Usando el corolario \ref{cor:rectangulo-cubriente} podemos encontrar una isometr\'ia local
\[ \psi:\{z\in\mathbb{C} \vert \re(z)\geq t_0, |\im(z)|<\delta\}\rightarrow X\]
donde \( \delta\) es menor que una tercio de la longitud de \( B\).

Si alguna de las trayectorias horizontales que parten de \( B\) converge a un punto cr\'itico infinito,
entonces como \( \gamma\) siempre est\'a a una distancia finita de dichas trayectorias, \( \gamma\) tambien
tendr\'ia que converger a dicho punto, lo cual es una contradicci\'on con que sea  divergente.
De hecho podemos refinar esta afirmaci\'on y asegurar que existe un abierto \(  U\) que contiene a todos los puntos cr\'iticos
infinitos y tal que la imagen de \( \psi\) es ajena a \(  U\). De esto \'ultimo se deduce que la imagen de \( \psi\) est\'a
contenida a una regi\'on de \'area finita.

Con esto demostrarmos que \( \psi\) no es inyectiva en ning\'un subrect\'angulo
\[{R_t:=\{z\in \mathbb{C} \vert t<\re(z) , |\im(z)|<\delta\} }\]
pues de lo contrario su imagen tendr\'ia \'area infinita.%
\footnote{Notemos que una vez que demostramos que \( \psi \) est\'a definida
para todo \( t>t_0\) y que su imagen est\'a contenida en una regi\'on de \'area
finita, la prueba es completamente an\'aloga al teorema de recurrencia de
Poincar\'e.}

Finalmente, si \( t>t_1\) es tal que la imagen bajo \( \psi\) del intervalo
vertical por el punto \( t\) intersecta al intervalo inicial, entonces, por la
elecci\'on de \( \delta\), podemos asegurar que \( \gamma(t)=\psi(t+i0)\) intersecta
a \( B\).%
\footnote{Para m\'as detalles de esta demostraci\'on, v\'ease el teorema \( 11.1\) en \cite[pag.~48]{strebel-quadratic}.}
\end{proof}

La proposici\'on anterior implica que toda trayectoria divergente es de hecho \emph{recurrente}, es decir, ella misma est\'a contenida en sus puntos l\'imite. Adem\'as, junto con el corolario \ref{cor:rectangulo-cubriente} son las piezas fundamentales
que permiten dar una clasificaci\'on expl\'icita de las vecindades de las
trayectorias horizontales:

\begin{proposicion}
	Sea \( \gamma\) una trayectoria horizontal. Entonces:
	\begin{itemize}
			 \item Si \( \gamma\) es gen\'erica, pertenece a una familia uniparam\'etrica
				 de trayectorias gen\'ericas, es decir, existe un biholomorfismo
				 \[ \psi: \{z\in\mathbb{C} \big\vert~ |\im(z)|<\epsilon \}\rightarrow  U\subseteq X\]
				 tal que \( \gamma = \psi(\mathbb{R})\) y \( \psi^{-1}\) es una carta normal.
			\item Si \( \gamma\) es peri\'odica, al igual que en el caso anterior, pertenece a una familia uniparam\'etrica
				de trayectorias peri\'odicas, todas de la misma longitud, es decir, existe una 
			       aplicacion cubriente y peri\'odica en la direcci\'on horizontal
				 \[ \psi: \{z\in \mathbb{C} \big\vert ~|\im(z)|<\epsilon\}\rightarrow  U\]
				 tal que \( \gamma = \psi(\mathbb{R})\) y localmente \( \psi^{-1}\) es una carta normal.
			\item Si \( \gamma\) es divergente, su cerradura es un dominio cuyo interior consta de trayectorias
				divergentes y su frontera es uni\'on de trayectorias silla y ceros de la diferencial.

	\end{itemize}
\end{proposicion}

Con esto obtenemos el resultado m\'as importante de este cap\'itulo:

\begin{teorema}\label{teo:descomposicion}
Sea \( C\) la uni\'on de todos los puntos cr\'iticos y las trayectorias silla y trayectorias
separatrices. Cada componente conexa de \( X\setminus C\) es equivalente a una
y solo una de las siguientes regiones:
\begin{itemize}
		 \item una \emph{franja horizontal}, es decir, la imagen bajo una isometr\'ia \( \psi\) de una franja del tipo
			 \[ \{z\in\mathbb{C}|0\leq\im(z)\leq h\}\]
			 donde la imagen de la frontera es una uni\'on de trayectorias silla y separatrices y \( \psi(z\pm t)\) converge a puntos cr\'iticos infinitos cuando \( t\to \infty\);
		\item un \emph{dominio anular}, es decir, la imagen bajo una isometr\'ia local \( \psi\)
			de una franja 
			 \[ \{z\in\mathbb{C}|a\leq\im(z)\leq b\}\]
			 tal que \( \psi\) es peri\'odica de periodo real. Si \( b<\infty\) entonces la imagen de la recta \( \{z\in \mathbb{C}|\im(z)=b\}\) es uni\'on de trayectorias silla.
			 De lo contrario, \( \psi(z+it)\) converge a un polo de orden \( 2\) cuando \( t\to\infty\). An\'alogamente en el caso de \( a\).
		\item un \emph{semiplano}, es decir, una franja horizontal en la que permitimos que
			\({ h=\infty}\). En este caso las trayectorias convergen a un polo de orden mayor que \( 2\);
		\item un \emph{dominio espiral}, es decir, el interior de la cerradura de una trayectoria divergente. La frontera de un dominio espiral es uni\'on de trayectorias silla.
\end{itemize}
\end{teorema}



\subsection{Diferenciales GMN y triangulaci\'on WKB (o de Stokes)}

A pesar de que el teorema \ref{teo:descomposicion} da una descripci\'on muy completa del
comportamiento global de las trayectorias horizontales de una diferencial cuadr\'atica,
en determinadas circunstancias queda lejos de ser manejable, principalmente en el caso
en que aparecen dominios espiral.

En general, se presenta aqu\'i una dicotom\'ia, en la que se pueden estudiar
diferenciales cuadr\'aticas con \'area finita, en cuyo caso las t\'enicas de
teor\'ia erg\'odica se vuelven las m\'as relevantes, o cuando las diferenciales
cuadr\'aticas tienen \'area infinita, en donde bajo ciertas condiciones
se pueden usar m\'etodos con tintes m\'as algebraicos o combinatorios.

En este trabajo nos enfocaremos en el segundo caso. M\'as espec\'ificamente,
adoptaremos las convenciones del art\'iculo \cite{bridgeland2015}.

\begin{definicion}
	Una diferencial \emph{GMN} es una diferencial cuadr\'atica meromorfa que cumple que:
	\begin{itemize}
			 \item todos sus ceros son simples,
			\item tiene al menos un polo,
			\item tiene al menos un punto cr\'itico finito.
	\end{itemize}
	Adem\'as, si una diferencial GMN no tiene puntos cr\'iticos infinitos diremos que es de \emph{\'area finita}
	o simplemente \emph{finita}. Por otro lado, si \'unicamente tiene puntos cr\'iticos infinitos diremos
	que es \emph{completa}, pues el complemento de dichos puntos es m\'etricamente completo.
\end{definicion}

Esta definici\'on aparece por primera vez en el art\'iculo \cite{neitzke-wall-crossing}.

M\'as a\'un, en la mayor\'ia de los casos de inter\'es para este trabajo, estudiaremos
diferenciales GMN que \'unicamente tienen polos dobles. Si adem\'as la diferencial
no tiene trayectorias silla, podemos definir la \emph{triangulaci\'on WKB}.%
\footnote{V\'ease tambi\'en el libro \cite{kawai} en donde aparece la definici\'on de la triangulaci\'on WKB para diferenciales sobre la esfera de Riemann.}

Antes de proseguir con este hecho, precisaremos del concepto de \emph{triangulaci\'on ideal},
una generalizaci\'on del concepto de triangulaci\'on:




%Una superficie con ponchaduras es una superficie diferenciable con un conjunto finito de puntos marcados
%a los que le llamaremos ponchaduras.
%Una triangulaci\'on ideal sobre una superficie con ponchaduras es una colecci\'on de clases de homotopia
%(relativa a los extremos)
%de arcos que conectan las ponchaduras que es maximal con respecto a la siguiente propiedad:
%las clases de homotopia son ajenas y se pueden escoger representantes que sean arcos
%inmersos en la superficie, ajenos dos a dos, y ajenos a las ponchaduras (salvo en los extremos).

%A una clase de equivalencia de arcos de la triangulaci\'on (o a un representante)
%le llamaremos simplemnte un arco de la triangulaci\'on. A las ponchaduras le llamaremos los v\'ertices de la triangulaci\'on.

\begin{definicion}
Una \emph{superficie con ponchaduras} es una superficie diferenciable y compacta con un conjunto finito de puntos marcados
a los que le llamaremos ponchaduras.
Una \emph{triangulaci\'on ideal} sobre una superficie con ponchaduras es una colecci\'on maximal de arcos
que conectan dos ponchaduras de la superficie (posiblemente la misma) y tal que ning\'un par de ellos es
homot\'opico relativo a los extremos ni se intersecta. Consideraremos como equivalentes a
cualquier par de triangulaciones cuyos arcos sean isot\'opicos relativamente a los extremos.
\end{definicion}

\begin{teorema}\label{teo:triangulacion-wkb} 
	Sea \( \varphi\) una diferencial GMN sobre \( X\) cuyos polos son todos de orden \( 2\)
	y no tiene trayectorias silla. Entonces, tomando como ponchaduras el conjunto de polos y como arcos 
	una trayectoria gen\'erica de cada franja horizontal, obtenemos una triangulaci\'on ideal de \( X\)
	a la que llamaremos \emph{triangulaci\'on WKB}.
\end{teorema}
\begin{proof}
	Como primera observaci\'on, notemos que la definici\'on de diferencial GMN implica que existe
	por lo menos un cero simple y un polo, y esto a su vez implica que existe por lo menos una franja horizontal.
	Adem\'as, las restricciones que hemos impuesto y el teorema \ref{teo:descomposicion} implican que
	las \'unicas regiones que aparecen al quitar las separatrices son franjas horizontales.

	Es claro que los arcos, que en este caso son las trayectorias gen\'ericas, son ajenos y conectan dos ponchaduras.
	Necesitamos verificar que los arcos no son homot\'opicos entre s\'i y que la colecci\'on de dichos arcos es maximal.

	Si dos trayectorias gen\'ericas que no pertenecen a la misma franja horizontal son homot\'opicas,
	entonces son frontera de un conjunto contra\'ible que no contiene ning\'un polo de la diferencial.
	Como no pertenecen a la misma franja horizontal,
	existe por lo menos un cero simple de la diferencial en dicho conjunto. Cada uno de los tres rayos
	que parten de dicho cero tiende a alguno de los dos polos a los que tienden las trayectorias gen\'ericas,
	por lo que dos de los rayos tienden al mismo polo.

	Llamemos \( R_1\) a la regi\'on acotada por dichos dos rayos, el polo y el cero.
	\( R_1\) es de nuevo un conjunto contra\'ible sin polos en el interior. Si \( R_1\) tampoco tuviese ceros en el interior,
	tendr\'ia que ser equivalente a un semiplano, pero sabemos que el punto l\'imite en los semiplanos necesariamente es
	un polo de orden mayor que \( 2\).

	Por otro lado, si existe un cero en \( R_1\) dicho cero tiene dos rayos que convergen al polo
	y que consecuentemente definen una nueva regi\'on \( R_2\) con las mismas propiedades que \( R_1\).
	Inductivamente, y dado que la cantidad de ceros de \( \varphi\) es finita, eventualmente llegamos a una contradicci\'on.

	Con esto demostramos que los arcos no son homot\'opicos.
	Demostraremos que la colecci\'on de arcos es maximal en la siguiente secci\'on,
	despu\'es de desarrollar las herramientas necesarias para calcular la cantidad de franjas horizontales de una diferencial dada.%\attention%
\footnote{Una demostraci\'on ligeramente distinta y m\'as general se puede encontrar en el lema \( 10.1\) de \cite{bridgeland2015}.}
\end{proof}



\tikzexternalenable
\begin{figure}
\centering
\tikzsetnextfilename{triangulacion_def}
\input{fig/triangulacion_def.pgf}
\caption{Trayectorias horizontales de una diferencial cuadr\'atica en el plano, con cuatro polos dobles y cuatro ceros simples.
En la imagen, las l\'ineas verdes son los arcos de la triangulaci\'on WKB.}\label{fig:triangulacion}
\end{figure}
\tikzexternaldisable
