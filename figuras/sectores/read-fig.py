# -*- coding: utf-8 -*-
import numpy as np
#import sys
#from scipy.integrate import odeint
#from cmath import *
#from multiprocessing import Pool
#######################################
#Definicion de la diferencial cuadratica
#def dq(z):
	#return (fase*((1/z**4+z-1+1/(z-complex(0.3,0.7))**3)*((z+1.2)**3))).conjugate() #diferencial cuadratica
	#return (fase*(z)).conjugate() #diferencial cuadratica
	#return (fase*(complex(-1,0)/z**2+(1-z)**5)).conjugate() diferencial cuadratica


#parametros

maxreps=2800 #repeticiones maximas de la rutina de integracion
fase=1.0 #fase de la diferencial
normav=0.1 #determina la norma del campo
normamax = 1.0 #determina el area dentro de la cual se integra el campo
t= np.linspace(0,0.020,5) #intervalo temporal
densidadPuntos=8 #determina cada cuantas repeticiones de la rutina de integracion se guarda un punto para su graficacion
colorLineas = 'k'
anchoLineas = 0.2
actualizaClick = 0#determina si se redibuja la figura cada click
dibujaEjes = 0
puntos = []
cuadros=2
#rotacion = 2*pi/cuadros
lim = 3
esfera=0
####matplotlib
import matplotlib as mpl
mpl.use("pgf")
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.collections import LineCollection
from mpl_toolkits import mplot3d as m3d
###Animacion


##Constantes e inicializacion de variables
#ri=sqrt(1j)
#ric=sqrt(-1j)
#monodromia=0
#ma=0
#patch1=0
#patch2=0
#pause = False

########### funciones #########
def estereografica(z):
	return [(2*z.real/(1+z*z.conjugate())).real,(2*z.imag/(1+z*z.conjugate())).real,((1-z*z.conjugate())/(1+z*z.conjugate())).real]
def mon(inicio, fin):
	global monodromia
	global ma
	global patch1
	global patch2
	if inicio.real<=0 and inicio.imag>0 and fin.real<=0 and fin.imag<0:
		monodromia=(monodromia + 1)%2
		ma=(ma+1)%2
		#print('m')
		#print(str(patch1)+str(patch2))
	if inicio.real<=0 and inicio.imag<0 and fin.real<=0 and fin.imag>0:
		monodromia=(monodromia + 1)%2
		ma=(ma+1)%2
		#print('-m')
		#print(str(patch1)+str(patch2))
	if (-1)*inicio.real>abs(inicio.imag):
		if fin.real<0 and fin.imag>(-1)*fin.real:
			patch1=(patch1+1)%2
			ma=0
		if fin.real<0 and fin.imag<fin.real:
			patch2=(patch2+1)%2
			ma=0
	if inicio.imag<0 and inicio.imag<inicio.real:
		if (-1)*fin.real>abs(fin.imag):
			patch2=(patch2+1)%2
	if inicio.imag>0 and inicio.imag>(-1)*inicio.real:
		if (-1)*fin.real>abs(fin.imag):
			patch1=(patch1+1)%2
	if patch1 and patch2:
		#print("igu")
		patch1=0
		patch2=0
	return
def dist(z):
	if patch1 != patch2:
		if patch1:
			r=ri*(sqrt(z*(-1j)))
		if patch2:
			r=ric*(sqrt(z*(1j)))
	else:
		r=sqrt(z)
	return (-1)**(monodromia+ma)*r

def f(y,t):
	xi=y[0]
	yi=y[1]
	#c=1
	z=dist(dq(complex(xi,yi)))
	#if abs(z)>10**4:
		#c=10.0**2/abs(z)
		#print c
	if z!=0:
		z=z/abs(z)*normav
	if abs(complex(xi,yi))>1:
		z=z*abs(complex(xi,yi))
	#z=z*c
	f0 = z.real
	f1 = z.imag
	return [f0, f1]

def fi(y,t):
	xi=y[0]
	yi=y[1]
	#c=1
	z=-dist(dq(complex(xi,yi)))
	#if abs(z)>10**4:
		#c=10.0**2/abs(z)
	if z!=0:
		z=z/abs(z)*normav
	if abs(complex(xi,yi))>1:
		z=z*abs(complex(xi,yi))
	#z=z*c
	f0 = z.real
	f1 = z.imag
	return [f0, f1]


def trayectp(z):
	global monodromia
	global patch1
	global patch2
	global ma
	monodromia, patch1, patch2, ma, = 0, 0, 0, 0
	norma=0.5
	#fin=z
	#x=(2*fin.real/(1+fin*fin.conjugate())).real
	#y=(2*fin.imag/(1+fin*fin.conjugate())).real
	#w=((1-fin*fin.conjugate())/(1+fin*fin.conjugate())).real
	if esfera==1:
		coord=np.array([estereografica(z)])
	else:
		coord=np.array([[z.real,z.imag]])
	#coord=np.array([[fin.real,fin.imag,0.0]])
	rep=0
	inicio=dq(z)
	fin=z
	if (-inicio.real)>abs(inicio.imag) and inicio.imag>0:
		patch1=1
	if (-inicio.real)>abs(inicio.imag) and inicio.imag<0:
		patch2=1
	inicio = z
	while (10**-5 < abs(dq(fin)) and norma < lim and rep<maxreps and abs(dq(fin))<10**5):
		sol = odeint(f,[inicio.real, inicio.imag],t,mxstep=5000)
		fin= complex(sol[-1,0],sol[-1,1])
		mon(dq(inicio),dq(fin))
		inicio = fin
		norma = max(inicio.real,inicio.imag)
		norma = abs(inicio)
		if rep%densidadPuntos==1:
			#x=(2*fin.real/(1+fin*fin.conjugate())).real
			#y=(2*fin.imag/(1+fin*fin.conjugate())).real
			#w=((1-fin*fin.conjugate())/(1+fin*fin.conjugate())).real
			if esfera==1:
				coord=np.append(coord,np.array([estereografica(fin)]),axis=0)
			else:
				coord=np.append(coord,np.array([[fin.real,fin.imag]]),axis=0)
			#coord=np.append(coord,np.array([[fin.real,fin.imag,0.0]]),axis=0)
			#np.append(coord,[[x,y,w]],axis=0)
		rep=rep+1
	#print('Trayectoria positiva, '+str(rep)+' repticiones')
	return coord 
def trayectn(z):
	global monodromia
	global patch1
	global patch2
	global ma
	monodromia, patch1, patch2, ma, = 0, 0, 0, 0
	norma=0.5
	#fin=z
	#x=(2*fin.real/(1+fin*fin.conjugate())).real
	#y=(2*fin.imag/(1+fin*fin.conjugate())).real
	#w=((1-fin*fin.conjugate())/(1+fin*fin.conjugate())).real
	if esfera==1:
		coord=np.array([estereografica(z)])
	else:
		coord=np.array([[z.real,z.imag]])
	#coord=np.array([[fin.real,fin.imag,0.0]])
	rep=0
	inicio=dq(z)
	fin=z
	if (-inicio.real)>abs(inicio.imag) and inicio.imag>0:
		patch1=1
	if (-inicio.real)>abs(inicio.imag) and inicio.imag<0:
		patch2=1
	inicio = z
	while (10**-5 < abs(dq(fin)) and norma < lim and rep<maxreps and abs(dq(fin))<10**5):
		sol = odeint(fi,[inicio.real, inicio.imag],t,mxstep=5000)
		fin= complex(sol[-1,0],sol[-1,1])
		mon(dq(inicio),dq(fin))
		inicio = fin
		norma = max(inicio.real,inicio.imag)
		norma = abs(inicio)
		if rep%densidadPuntos==1:
			if esfera==1:
				coord=np.append(coord,np.array([estereografica(fin)]),axis=0)
			else:
				coord=np.append(coord,np.array([[fin.real,fin.imag]]),axis=0)
			#x=(2*fin.real/(1+fin*fin.conjugate())).real
			#y=(2*fin.imag/(1+fin*fin.conjugate())).real
			#w=((1-fin*fin.conjugate())/(1+fin*fin.conjugate())).real
			#coord=np.append(coord,np.array([[fin.real,fin.imag,0.0]]),axis=0)
		rep=rep+1
	#print('Trayectoria negativa, '+str(rep)+' repticiones')
	return coord
def trayectoria(z):
	tn=trayectn(z)
	tp=trayectp(z)
	total=np.vstack((tn[::-1],tp))
	#x=tn[0]
	#x.reverse()
	#x.extend(tp[0])
	#y=tn[1]
	#y.reverse()
	#y.extend(tp[1])
	#z=tn[2]
	#z.reverse()
	#z.extend(tp[2])
	#print('.'),
	sys.stdout.write(".")
	sys.stdout.flush()
	return total

#interfaz
#def onpress(event):
	#global fase
	#global pause
	#if event.key=='d':
		#fase = fase*rect(1,-0.1)
		#col.set_segments(trayectorias())
		#fig.canvas.draw()
		#print('detecto a')
		#print(fase)
	#if event.key=='a':
		#print(str(len(puntos))+' puntos seleccionados')
		#pause = True
		##anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
		#anim.save('basic_animation.mp4',bitrate=3000)
	#if event.key=='r':
		#print('redibujando...')
		#col.set_segments(coleccionTrayectorias)
		#fig.canvas.draw()
	#if event.key=='e':
		#plt.savefig('figura.pgf')

#def trayectorias(listaPuntos):
	#tray=[]
	#for p in listaPuntos:
		#tray.append(trayectoria(p))
		#sys.stdout.write(".")
		#sys.stdout.flush()
	#return np.array(tray)
#frame=0



####### generacion manual de los puntos

#for x in np.linspace(-3,3.9,20):
	#for y in np.linspace(-3,3,20):
		#puntos.append(complex(-x,y))
	#puntos.append(complex(x,0)*rect(1,pi/3+0.1))
	#puntos.append(complex(x,0)*rect(1,-pi/3+0.1))
#pool=Pool()
#resTray = pool.map(trayectoria,puntos)
#pool.close()
#pool.join()
#for curvaPrueb in resTray:
	#coleccionTrayectorias.append(list(zip(curvaPrueb[0],curvaPrueb[1],curvaPrueb[2])))
##for p in puntos:
	##curvaPrueb=trayectoria(p)
	##coleccionTrayectorias.append(list(zip(curvaPrueb[0],curvaPrueb[1],curvaPrueb[2])))
###########
#col.set_segments(coleccionTrayectorias)

#def calculaCuadro(i):
	#fase=rect(1,i*rotacion)
	#print('cuadro '+str(i))
	#return trayectorias(puntos)
#pool=Pool()
#anim=pool.map(calculaCuadro, range(0,cuadros))
#pool.close()
#pool.join()

#sys.stdout.write("\n Escribiendo resultados\n")
#resultado=np.array(anim)
#np.save("anim.npy",resultado)

#def onclick(event):
	#if event.button == 2:
		#global coleccionTrayectorias
		#curvaPrueb=trayectoria(complex(event.xdata,event.ydata))
		#coleccionTrayectorias.append(list(zip(curvaPrueb[0],curvaPrueb[1])))
		#puntos.append(complex(event.xdata,event.ydata))
		#print('Click en la coordenada: '+str(event.xdata)+str(event.ydata)+' en donde la diferencial vale: '+str(dq(complex(event.xdata,event.ydata))))
		#if actualizaClick ==1:
			#col.set_segments(coleccionTrayectorias)
			#fig.canvas.draw()
#fig.canvas.mpl_connect('key_press_event', onpress)
#fig.canvas.mpl_connect('button_press_event', onclick)
##mostrar el ambiente

#def animate(i):
	#global fase, frame
	#if pause:
		#fase = fase*rect(1,rotacion)	
		#col.set_segments(trayectorias(puntos))
		#frame = frame+1
		#print(' Cuadro '+str(frame)+' de 50 completado')
	#return col,

##anim = animation.FuncAnimation(fig, animate, init_func=init,
                               ##frames=200, interval=50, blit=True)
#pool=Pool(1)
#resTray = pool.map(trayectoria,puntos)
#pool.close()
#pool.join()
#lista=[]
#for x in puntos:
	#lista.append(trayectoria(x))

#inicializacion de la figura
#np.save("hoyo.npy",np.array(resTray))
#if esfera==1:
	#ax = fig.add_subplot(111, xlim=(-1,1), ylim=(-1,1), zlim=(-1,1), projection='3d')
	#col = m3d.art3d.Line3DCollection(resTray,color=colorLineas,linewidths=anchoLineas,antialiaseds=True)
	#ax.add_collection3d(col)
#else:
#ax = fig.add_subplot(111, xlim=(-2,2), ylim=(-2,2), autoscale_on=False)
#ax.get_xaxis().set_visible(dibujaEjes)
#ax.get_yaxis().set_visible(dibujaEjes)
#offs= (0.0, 0.0)
#col = LineCollection(resTray,offsets=offs,color=colorLineas,linewidths=anchoLineas,antialiaseds=True)
#ax.add_collection(col, autolim=True)

#plt.savefig('figure.pgf')

#plt.show()

fig = plt.figure()
fig.set_size_inches(4, 4, forward=True)
ax = fig.add_subplot(111, xlim=(-2,2), ylim=(-2,2), aspect='equal', autoscale_on=False)
fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=None, hspace=None)

#ax = fig.add_subplot(111, xlim=(-2,2), ylim=(-2,2), aspect='equal', autoscale_on=False)
ax.get_xaxis().set_visible(dibujaEjes)
ax.get_yaxis().set_visible(dibujaEjes)
offs= (0.0, 0.0)
col = LineCollection([],offsets=offs,color=colorLineas,linewidths=anchoLineas,antialiaseds=True)
ax.add_collection(col, autolim=True)

#col.set_segments(readFig)


#sect=np.load("sect3.npy")

##for x in range(0,6):
	##ax.add_collection(LineCollection(sect[x],offsets=offs,color=colorLineas,linewidths=anchoLineas,antialiaseds=True))


	
#ax.add_collection(LineCollection(sect[0],offsets=offs,color='b',linewidths=anchoLineas,antialiaseds=True))
##ax.add_collection(LineCollection(sect[3],offsets=offs,color='b',linewidths=anchoLineas,antialiaseds=True))
#ax.add_collection(LineCollection(sect[1],offsets=offs,color='r',linewidths=anchoLineas,antialiaseds=True))
##ax.add_collection(LineCollection(sect[4],offsets=offs,color='r',linewidths=anchoLineas,antialiaseds=True))
#ax.add_collection(LineCollection(sect[2],offsets=offs,color='g',linewidths=anchoLineas,antialiaseds=True))
#ax.add_collection(LineCollection(sect[5],offsets=offs,color='g',linewidths=anchoLineas,antialiaseds=True))

#plt.savefig('sect3.pgf')



for n in range(1,5):
	sect=np.load("sect"+str(n)+".npy")
	for i in range(0,n):
		ax.add_collection(LineCollection(sect[i],offsets=offs,color='k',linewidths=anchoLineas,antialiaseds=True))
	plt.savefig('sector'+str(n)+'.pgf')
	#fig.draw()
	plt.cla()
#for x in range(-5,5):
	#readFig=np.load("fig"+str(x)+".npy")
	#col.set_segments(readFig)
	#plt.savefig("z"+str(x)+".pgf")
	
#def init():
    #col.set_segments([])
    #return col,




#def animate(i):
	#col.set_segments(readAnim[i])
	#return col,


#anim = animation.FuncAnimation(fig, animate, init_func=init,
                               #frames=300, interval=50)

#anim.save('hoyo.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
#anim.save('hoyo300.mp4',fps=30,bitrate=6000)
plt.show()
